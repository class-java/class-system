package com.flushbonading;


import com.flushbonading.mapper.*;
import com.flushbonading.service.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ClassSystemApplicationTests {
    @Autowired
    private IScoreService scoreService;
    //    @Autowired
//    private RedisClientTest redisCli;
    @Autowired
    private ICourseService courseService;
    @Autowired
    private CourseMapper courseMapper;
    @Autowired
    private ITeacherService teacherService;
    @Autowired
    private TeacherMapper teacherMapper;
    @Autowired
    private ScoreMapper scoreMapper;
    @Autowired
    private StudentMapper studentMapper;
    @Autowired
    private ProjectMapper projectMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private IMenuService menuService;
    @Autowired
    private ITimeTableService timeTableService;
    @Autowired
    private IAssociationService associationService;


    @Test
    public void testRedis() {
        final int countsByStudentNumber = this.associationService.getCountsByStudentNumber("");
        System.out.println(countsByStudentNumber);
    }


}
