//package com.flushbonading;
//
//import com.flushbonading.service.IUserService;
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.StringRedisTemplate;
//import org.springframework.stereotype.Component;
//
///**
// * @author zyx 2134208960@qq.com
// * @date 2021/4/19 11:34
// * @since 1.0.0
// */
//@Component
//public class RedisClientTest {
//    private StringRedisTemplate template;
//
//    @Autowired
//    public RedisClientTest(StringRedisTemplate template) {
//        this.template = template;
//    }
//
//    public StringRedisTemplate getTemplate() {
//        return template;
//    }
//
//}
