package com.flushbonading.controller;

import com.flushbonading.annotation.BackStage;
import com.flushbonading.annotation.PassToken;
import com.flushbonading.pojo.RespBean;
import com.flushbonading.pojo.User;
import com.flushbonading.pojo.dto.UserDTO;
import com.flushbonading.service.impl.UserServiceImpl;
import com.flushbonading.util.TokenUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;


/**
 * @Author zlx&zyx
 * @description
 * @Version 1.0
 * @Date 2021/4/8 20:31
 */
@RestController
@RequestMapping("/Login")
@Api(value = "登录模块", tags = "登录接口")
@BackStage
public class LoginController {
    @Autowired
    private UserServiceImpl userService;

    @ApiOperation(value = "登录")
    @PassToken
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public RespBean userLogin(final HttpServletRequest request, @RequestBody final UserDTO userDTO) {

        System.out.println(userDTO);
        final User user = this.userService.Login(userDTO);
        final HttpSession session = request.getSession();
        if (user == null) {
            session.setAttribute("isError", true);
            return RespBean.error("isError");
        } else {
            final Map<String, Object> tokenMap = new HashMap<>();
            tokenMap.put("token", TokenUtils.getUserToken(user));
            System.out.println(TokenUtils.getUserToken(user));
            System.out.println(tokenMap);
            tokenMap.put("user", user);
            return RespBean.success("登录成功", tokenMap);
        }

    }


//    @UserLoginToken
//    @PostMapping("/getmessage")
//    public RespBean getmessage(){
//        String aaa="hello word";
//        return RespBean.success("你已经通过验证",aaa);
//    }


}
