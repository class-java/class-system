package com.flushbonading.controller;

import com.flushbonading.annotation.BackStage;
import com.flushbonading.pojo.RespBean;
import com.flushbonading.pojo.dto.FileRegistrationDTO;
import com.flushbonading.pojo.dto.RegistrationTempDTO;
import com.flushbonading.service.RegistrationService;
import com.flushbonading.util.FileRegistrationUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author zk
 * @date 2021/9/24 17:07
 */
@RestController
@RequestMapping("/face/registration")
@BackStage
@Api(value = "嵌入式报名模块", tags = "学生报名")
public class RegistrationDtoController {
    @Autowired
    private RegistrationService registrationService;

    /**
     * 添加报名信息
     */
    @ApiOperation(value = "添加报名信息")
    @PostMapping("/join")
    public RespBean insertRegistration(@RequestBody final RegistrationTempDTO registrationTempDTO) {
        final RespBean respBean = this.registrationService.insertRegistration(registrationTempDTO);
        System.out.println(respBean);
        return respBean;
    }

    /**
     * 查看报名信息
     */
    @ApiOperation(value = "查看报名信息")
    @PostMapping("/all")
    public List<RegistrationTempDTO> getAll() {
        return this.registrationService.getRegistrationTempDto();
    }

    /**
     * 录取报名学生
     */
    @ApiOperation(value = "录取报名学生")
    @PostMapping("/admit")
    public RespBean updateRegistrationAdmit(@RequestBody final List<RegistrationTempDTO> registrationTempDTO) {
        final RespBean respBean = this.registrationService.updateRegistrationAdmit(registrationTempDTO);
        System.out.println(respBean);
        return respBean;
    }

    @ApiOperation(value = "修改报名信息")
    @PostMapping("/update")
    public RespBean updateRegistrationData(@RequestBody final RegistrationTempDTO registrationTempDTO) {
        final RespBean respBean = this.registrationService.updateRegistrationData(registrationTempDTO);
        System.out.println(respBean);
        return respBean;
    }

    /**
     * 将报名信息导出Excel
     */
    @ApiOperation(value = "将报名信息导出Excel")
    @GetMapping("/download")
    public void downloadExcel(final HttpServletResponse response) throws IOException {
        final List<FileRegistrationDTO> fileRegistrationDTOList = this.registrationService.getFileRegistrationDto();
        FileRegistrationUtil.writeExcel(response, fileRegistrationDTOList);
    }
}
