package com.flushbonading.controller;


import com.flushbonading.annotation.BackStage;
import com.flushbonading.pojo.RespBean;
import com.flushbonading.pojo.SystemConfig;
import com.flushbonading.service.SystemConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zk
 * @since 2021-10-11
 */
@RestController
@RequestMapping("/system-config")
@BackStage
@Api(value = "系统配置信息模块")
public class SystemConfigController {
    @Autowired
    private SystemConfigService systemConfigService;

    @ApiOperation(value = "添加或更新配置信息")
    @PostMapping("/inUp")
    private RespBean installConfig(@RequestBody final SystemConfig systemConfig) {
        System.out.println(systemConfig.toString());
        final RespBean respBean = this.systemConfigService.insertConfigInformation(systemConfig);
        System.out.println(respBean);
        return respBean;
    }
}

