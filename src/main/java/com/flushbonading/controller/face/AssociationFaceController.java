package com.flushbonading.controller.face;

import com.flushbonading.pojo.RespBean;
import com.flushbonading.pojo.dto.AssociationDTO;
import com.flushbonading.service.IAssociationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/18 20:14
 * @since 1.0.0
 */
@RestController
@RequestMapping("/face/association")
@Api(tags = "协会相关接口(门户)")
public class AssociationFaceController {

    @Autowired
    private IAssociationService associationService;

    /**
     * @param associationDTO 从前端传过来的加入协会表单对象
     * @return RespBean对象
     */
    @PostMapping("/join")
    @ApiOperation("加入协会接口")
    @ApiImplicitParam(name = "associationDTO", value = "从前端传过来的加入协会表单对象")
    public RespBean joinAssociation(@RequestBody final AssociationDTO associationDTO) {
        /*
            先判断该学生是否在协会里
         */
        if (this.associationService.getCountsByStudentNumber(associationDTO.getNum()) > 0) {
            return RespBean.error("您已经在协会中了");
        }

        /*
            如果不在，则进行插入
         */
        final Integer student = this.associationService.insertAStudent(associationDTO);
        if (student == null) {
            return RespBean.error("表单提交失败");
        }
        return RespBean.success("表单提交成功");
    }
}
