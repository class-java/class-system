package com.flushbonading.controller;


import com.flushbonading.annotation.BackStage;
import com.flushbonading.pojo.dto.TeacherDTO;
import com.flushbonading.service.ITeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
@RestController
@RequestMapping("/teacher")
@BackStage
public class TeacherController {
    @Autowired
    private ITeacherService teacherService;


    @PostMapping("/all")
    public List<TeacherDTO> getAllTeacher() {
        return this.teacherService.getAllTeacher();
    }
}
