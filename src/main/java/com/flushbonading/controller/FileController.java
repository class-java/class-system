package com.flushbonading.controller;

import com.flushbonading.annotation.BackStage;
import com.flushbonading.service.impl.RegistrationServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zyx 2134208960@qq.com
 * @version 0.1.0
 * @create 2021-03-15 09:03
 * @since 0.1.0
 **/
@RestController
@Slf4j
@BackStage
public class FileController {
    @Autowired
    RegistrationServiceImpl registrationService;

}
