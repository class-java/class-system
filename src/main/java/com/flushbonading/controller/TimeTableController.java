package com.flushbonading.controller;

import com.flushbonading.annotation.BackStage;
import com.flushbonading.pojo.RespBean;
import com.flushbonading.service.ITimeTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/4 18:54
 * @since 1.0.0
 */
@RestController
@RequestMapping("/timetable")
@BackStage
public class TimeTableController {
    @Autowired
    private ITimeTableService timeTableService;

    @PostMapping("/select")
    public String selectPath() {
        return this.timeTableService.selectPath();
    }

    @PostMapping("/edit")
    public RespBean updatePath(@RequestBody String path) {
        try {
            path = java.net.URLDecoder.decode(path, "UTF-8");
        } catch (final UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        path = path.substring(0, path.length() - 1);
        final int i = this.timeTableService.updatePath(path);
        if (i > 0) {
            return RespBean.success("上传成功");
        }
        return RespBean.error("上传失败");

    }
}
