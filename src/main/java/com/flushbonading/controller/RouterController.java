package com.flushbonading.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author zyx 2134208960@qq.com
 * @version 0.1.0
 * @create 2021-03-16 09:50
 * @since 0.1.0
 **/
@Controller
public class RouterController {
    @GetMapping({"/index", "/"})
    public String index(){
        return "index";
    }
    @GetMapping("/tologin")
    public String tologin(){
        return "loginPage";
    }
    @GetMapping("/end")
    public String end(){
        return "end";
    }
}
