package com.flushbonading.controller;

import cn.hutool.crypto.SecureUtil;
import com.auth0.jwt.JWT;
import com.flushbonading.annotation.BackStage;
import com.flushbonading.annotation.UserLoginToken;
import com.flushbonading.pojo.RespBean;
import com.flushbonading.pojo.Student;
import com.flushbonading.pojo.User;
import com.flushbonading.pojo.dto.StudentDTO;
import com.flushbonading.pojo.dto.StudentMessageDTO;
import com.flushbonading.service.IProjectService;
import com.flushbonading.service.IStudentService;
import com.flushbonading.service.IStudyDirectionService;
import com.flushbonading.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zlx&zyx
 * @since 2021-04-07
 */
@RestController
@RequestMapping("/student")
@Api(value = "学生模块", tags = "学生接口")
@BackStage
public class StudentController {
    @Autowired
    private IStudentService studentService;
    @Autowired
    private IStudyDirectionService studyDirectionService;
    @Autowired
    private IProjectService projectService;
    @Autowired
    private IUserService userService;

    @UserLoginToken
    @ApiOperation(value = "用过userid获得学生个人信息")
    @GetMapping("/add")
    public RespBean getStudent(@RequestParam final String user_id) {
        final Student student = this.studentService.getStudent(user_id);
        student.setStudy_direction(this.studyDirectionService.selectStudyDirection(student.getStudy_direction_id()));
        student.setProject(this.projectService.selectProjectByProject(student.getProject_id()));
        System.out.println(student);
        return RespBean.success("获得成功", student);
    }


    @UserLoginToken
    @ApiOperation(value = "学生修改个人信息")
    @PostMapping("/studentUpdate")
    public RespBean studetnUpdateStudent(@RequestBody final Student student) {
        final User user = User.builder().user_id(student.getStudent_number()).password(SecureUtil.md5(student.getPassword())).build();
        this.userService.updateUser(user);
        student.setPassword(SecureUtil.md5(student.getPassword()));
        System.out.println(student);
        if (this.studentService.updateById(student)) {
            return RespBean.success("更新成功,请重新登录");
        }
        return RespBean.success("更新失败");
    }

    @PostMapping("/studentMessage")
    public StudentDTO selectStudentMessage(final HttpServletRequest request) {
        final String token = request.getHeader("token");
        final String user_id = JWT.decode(token).getAudience().get(0);
        return this.studentService.selectStudentMessage(user_id);
    }

    @PostMapping("/update")
    public RespBean updateStudentMessage(@RequestBody final StudentDTO studentDTO, final HttpServletRequest request) {
        final String token = request.getHeader("token");
        final String user_id = JWT.decode(token).getAudience().get(0);
        final int i = this.studentService.updateStudentMessage(studentDTO, user_id);
        if (i > 0) {
            return RespBean.success("个人信息修改成功");
        }
        return RespBean.error("对不起，您的信息修改失败");
    }

    @PostMapping("/getName")
    public String getStudentName(final HttpServletRequest request) {
        final String token = request.getHeader("token");
        final String user_id = JWT.decode(token).getAudience().get(0);
        return this.studentService.getName(user_id);
    }

    @PostMapping("/select/all")
    public List<StudentMessageDTO> getAllStudent() {
        final List<StudentMessageDTO> allStudent = this.studentService.getAllStudent();
        if (allStudent.size() <= 0) {
            allStudent.add(StudentMessageDTO.builder().build());
        }
        return allStudent;
    }


    @PostMapping("/detailMessage/{student_number}")
    public StudentDTO detailMessage(@PathVariable("student_number") final String student_number) {
        return this.studentService.selectStudentMessage(student_number);
    }

    @ApiOperation("删除学生信息")
    @PostMapping("/delete/{student_number}")
    public RespBean deleteAStudent(@PathVariable("student_number") final String student_number) {
        final Integer projectID = this.studentService.getProjectIDByStudentNumber(student_number);
        if (projectID == null) {
            final int i = this.studentService.deleteAStudent(student_number);
            if (i >= 0) {
                final int j = this.userService.deleteAUser(student_number);
                if (j >= 0) {
                    return RespBean.success("删除成功");
                } else {
                    return RespBean.error("删除失败");
                }
            }
            return RespBean.error("删除失败");
        }
        return RespBean.error("请先从项目里移除此人");
    }

    @ApiOperation("修改学生信息")
    @PostMapping("/updateStudent")
    public RespBean updateStudentMessageByStudentNumber(@RequestBody final StudentMessageDTO studentMessageDTO) {
        final int i = this.studentService.updateStudentMessageByStudentNumber(studentMessageDTO);
        if (i > 0) {
            return RespBean.success("修改成功");
        }
        return RespBean.error("修改失败");
    }

    @ApiOperation("添加学生")
    @PostMapping("/insert")
    public RespBean insertAStudent(@RequestBody final StudentMessageDTO studentMessageDTO) {
        final int i = this.studentService.insertAStudent(studentMessageDTO);
        if (i > 0) {
            return RespBean.success("新增学生成功");
        }
        return RespBean.error("插入学生失败");
    }

    @ApiOperation("查询功能")
    @PostMapping("/search")
    public List<StudentMessageDTO> searchByName(@RequestBody String name) {
        try {
            name = java.net.URLDecoder.decode(name, "UTF-8");
        } catch (final UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        name = name.substring(0, name.length() - 1);
        System.out.println(name);
        return this.studentService.searchByName(name);
    }

}
