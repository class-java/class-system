package com.flushbonading.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/9 8:58
 * @since 1.0.0
 */
@RestController
@RequestMapping("/Logout")
public class LogoutController {
    @PostMapping("/logout")
    public void logout(final HttpServletRequest request) {
        final String token = request.getHeader("token");
        System.out.println("token:" + token);
    }
}
