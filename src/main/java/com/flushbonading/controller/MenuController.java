package com.flushbonading.controller;


import com.auth0.jwt.JWT;
import com.flushbonading.annotation.BackStage;
import com.flushbonading.annotation.UserLoginToken;
import com.flushbonading.pojo.Menu;
import com.flushbonading.pojo.User;
import com.flushbonading.pojo.dto.MenuDTO;
import com.flushbonading.service.IMenuService;
import com.flushbonading.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
@RestController
@RequestMapping("/menu")
@Api(value = "菜单模块", tags = "菜单接口")
@BackStage
public class MenuController {
    @Autowired
    private IMenuService menuService;
    @Autowired
    private IUserService userService;

    @UserLoginToken
    @ApiOperation(value = "传入用户id获得菜单")
    @PostMapping("/all")
    @Cacheable("menu")
    public List<Menu> getMenu(final HttpServletRequest request) {
        final String token = request.getHeader("token");
        final String user_id = JWT.decode(token).getAudience().get(0);
        final User role = this.userService.getRoleByUserId(user_id);
        final List<Menu> menus = this.menuService.getRoleMenu(role.getRole_id());
        handleMenus(menus);
        return menus;
    }

    public void handleMenus(final List<Menu> menus) {
        for (final Menu menu : menus) {
            final List<Menu> children = getMenus(menu.getMenu_id(), menus);
            if (children.size() > 0) {
                menu.setChildren(children);
            }

        }

        final Iterator<Menu> iterator = menus.iterator();
        while (iterator.hasNext()) {
            final Menu menu = iterator.next();
            if (menu.getParent_id() != 0) {
                iterator.remove();
            }
        }
    }

    private List<Menu> getMenus(final Integer menu_id, final List<Menu> menus) {
        final List<Menu> children = new ArrayList<>();
        for (final Menu menu : menus) {
            if (menu.getParent_id() == menu_id) {
                children.add(menu);
            }
        }
        return children;
    }

    @PostMapping("/permission/{role_id}")
    public List<MenuDTO> getPermission(@PathVariable("role_id") final int role_id) {
        final List<MenuDTO> menuDTOS = this.menuService.getAllPermission();
        final List<MenuDTO> menuDTOS1 = this.menuService.selectMenuByRoleID(role_id);
        for (final MenuDTO menus : menuDTOS) {
            if (menuDTOS1.contains(menus))
                menus.setChecked(true);
        }
        menuDTOS.forEach(System.out::println);
        handleMenusDTO(menuDTOS);
        for (final MenuDTO menus : menuDTOS) {
            final List<MenuDTO> children = menus.getChildren();
            if (children != null) {
                menus.setChecked(false);
                for (final MenuDTO ch : children) {
                    if (ch.getChildren() != null)
                        ch.setChecked(false);
                }
            }
        }
        System.out.println();
        menuDTOS.forEach(System.out::println);
        return menuDTOS;
    }

    public void handleMenusDTO(final List<MenuDTO> menus) {
        for (final MenuDTO menu : menus) {
            final List<MenuDTO> children = getMenusDTO(menu.getId(), menus);
            if (children.size() > 0) {
                menu.setChildren(children);
            }

        }

        final Iterator<MenuDTO> iterator = menus.iterator();
        while (iterator.hasNext()) {
            final MenuDTO menu = iterator.next();
            if (menu.getParent_id() != 0) {
                iterator.remove();
            }
        }
    }

    private List<MenuDTO> getMenusDTO(final Integer menu_id, final List<MenuDTO> menus) {
        final List<MenuDTO> children = new ArrayList<>();
        for (final MenuDTO menu : menus) {
            if (menu.getParent_id() == menu_id) {
                children.add(menu);
            }
        }
        return children;
    }
}
