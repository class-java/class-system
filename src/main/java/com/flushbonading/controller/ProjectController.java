package com.flushbonading.controller;


import com.auth0.jwt.JWT;
import com.flushbonading.annotation.BackStage;
import com.flushbonading.pojo.RespBean;
import com.flushbonading.pojo.dto.ProjectDTO;
import com.flushbonading.pojo.dto.ProjectListDAO;
import com.flushbonading.pojo.dto.ProjectStudentDTO;
import com.flushbonading.service.IProjectService;
import com.flushbonading.service.IStudentService;
import com.flushbonading.util.OperatingPostUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zlx&zyx
 * @since 2021-04-07
 */
@RestController
@RequestMapping("/project")
@Api(value = "项目模块", tags = "项目接口")
@BackStage
public class ProjectController {
    @Autowired
    private IProjectService projectService;
    @Autowired
    private IStudentService studentService;

    @PostMapping("/all")
    public List<ProjectDTO> selectAll() {
        return this.projectService.selectAll();
    }

    @PostMapping("/insert")
    public RespBean insertAProject(@RequestBody final ProjectDTO projectDTO) {
        final int index = this.projectService.getLastProjectID();
        projectDTO.setProject_id(index + 1);
        final int i = this.projectService.insertAProject(projectDTO);
        if (i > 0) {
            return RespBean.success("项目填加成功");
        }
        return RespBean.error("对不起，项目填加失败");
    }

    @PostMapping("/delete/{project_id}")
    public RespBean deleteProject(@PathVariable("project_id") final int project_id) {
        final int i = this.studentService.updateProjectID(project_id);
        final int j = this.projectService.deleteProject(project_id);
        if (i >= 0 && j > 0) {
            return RespBean.success("删除项目成功");
        }
        return RespBean.error("对不起,删除项目失败");
    }

    @PostMapping("/add/{project_id}")
    public RespBean addAStudent(@PathVariable("project_id") final int project_id, @RequestBody final ProjectStudentDTO projectStudentDTO) {
        final int counts = this.studentService.atTheProjectCounts(projectStudentDTO.getStudent_number());
        if (counts > 0) {
            return RespBean.error("该学生已在某个项目中");
        }
        final int i = this.projectService.updateGroupName(project_id, projectStudentDTO.getGroup_name());
        final int j = this.studentService.updateProject(project_id, projectStudentDTO);
        System.out.println(projectStudentDTO);
        if (i > 0 && j > 0) {
            return RespBean.success("添加成功");
        }
        return RespBean.error("添加失败");
    }

    @PostMapping("/select/{project_id}")
    public List<ProjectStudentDTO> queryStudent(@PathVariable("project_id") final int project_id) {
        final List<ProjectStudentDTO> projectStudentDTOS = this.projectService.selectStudentByID(project_id);
        if (projectStudentDTOS.size() <= 0) {
            projectStudentDTOS.add(ProjectStudentDTO.builder().build());
        }
        return projectStudentDTOS;
    }

    @PostMapping("/deleteStudent/{student_number}")
    public RespBean deleteAStudent(@PathVariable("student_number") final String student_number) {
        final int i = this.projectService.deleteAStudent(student_number);
        if (i >= 0) {
            return RespBean.success("删除成功");
        }
        return RespBean.error("删除失败");
    }

    @PostMapping("/update/{project_id}")
    public RespBean updateProject(@PathVariable("project_id") final int project_id, @RequestBody final ProjectDTO projectDTO) {
        System.out.println(projectDTO);
        final int i = this.projectService.updateProject(project_id, projectDTO);
        if (i > 0) {
            return RespBean.success("项目修改成功");
        }
        return RespBean.error("项目修改失败");
    }

    @PostMapping("/select/student")
    public String selectIntroduction(final HttpServletRequest request) {
        final String token = request.getHeader("token");
        final String user_id = JWT.decode(token).getAudience().get(0);
        return this.projectService.selectIntroduction(user_id);
    }

    @PostMapping("/update/student/{project_id}")
    public RespBean updateStudent(@RequestBody final ProjectStudentDTO projectStudentDTO, @PathVariable("project_id") final int project_id) {
        final int i = this.projectService.updateStudent(projectStudentDTO, project_id, OperatingPostUtil.operatingPostID(projectStudentDTO.getJob_position()));
        if (i > 0) {
            return RespBean.success("修改成功");
        }
        return RespBean.error("修改失败");
    }

    @PostMapping("/selectAll")
    public List<ProjectListDAO> selectAllProjects() {
        return this.projectService.selectAllProjects();
    }

}
