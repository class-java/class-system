package com.flushbonading.controller;

import com.auth0.jwt.JWT;
import com.flushbonading.annotation.BackStage;
import com.flushbonading.pojo.RespBean;
import com.flushbonading.pojo.dto.ScheduleDTO;
import com.flushbonading.service.IProjectScheduleService;
import com.flushbonading.service.IStudentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/12 9:52
 * @since 1.0.0
 */
@RestController
@RequestMapping("/projectSchedule")
@BackStage
public class ProjectScheduleController {
    @Autowired
    private IProjectScheduleService projectScheduleService;
    @Autowired
    private IStudentService studentService;

    @PostMapping("/upload")
    public RespBean uploadSchedule(final HttpServletRequest request, @RequestBody final ScheduleDTO scheduleDTO) {
        final String token = request.getHeader("token");
        final String user_id = JWT.decode(token).getAudience().get(0);
        final Integer projectID = this.studentService.getProjectIDByStudentNumber(user_id);
        if (projectID == null)
            return RespBean.error("该学生没有加入任何项目");
        scheduleDTO.setProject_id(projectID);
        final int i = this.projectScheduleService.insertSchedule(scheduleDTO);
        if (i > 0)
            return RespBean.success("项目进度上传成功");
        return RespBean.success("上传失败");
    }

    @PostMapping("/all/{project_id}")
    public List<ScheduleDTO> getAll(@PathVariable("project_id") final int project_id) {
        return this.projectScheduleService.getAllSchedule(project_id);
    }

    @PostMapping("/select")
    public List<ScheduleDTO> getAllScheduleByStudentNumber(final HttpServletRequest request) {
        final String token = request.getHeader("token");
        final String user_id = JWT.decode(token).getAudience().get(0);
        return this.projectScheduleService.getAllScheduleByStudentNumber(user_id);
    }

    @ApiOperation("删除某个进度")
    @PostMapping("/delete")
    public RespBean deleteASchedule(@RequestBody final ScheduleDTO scheduleDTO) {
        final int i = this.projectScheduleService.deleteASchedule(scheduleDTO);
        if (i >= 0)
            return RespBean.success("删除成功");
        return RespBean.error("删除失败");
    }
}
