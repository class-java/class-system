package com.flushbonading.controller;


import com.flushbonading.annotation.BackStage;
import com.flushbonading.pojo.RespBean;
import com.flushbonading.pojo.dto.EnrollDTO;
import com.flushbonading.pojo.dto.RegistrationDTO;
import com.flushbonading.service.impl.RegistrationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zyx
 * @since 2021-04-07
 */
@RestController
@RequestMapping("/registration")
@BackStage
public class RegistrationController {
    @Autowired
    private RegistrationServiceImpl registrationService;

    @ResponseBody
    @GetMapping("/enroll")
    public boolean enroll(final EnrollDTO enrollDTO) {
        return this.registrationService.enroll(enrollDTO);
    }

    @PostMapping("/all")
    public List<RegistrationDTO> getAll() {
        return this.registrationService.getRegistrationMessage();
    }

    @PostMapping("/delete/{student_number}")
    public RespBean delete(@PathVariable("student_number") final String student_number) {
        System.out.println("进入了");
        final int i = this.registrationService.deleteRegistrationByStudentNum(student_number);
        if (i < 0)
            return RespBean.error("删除失败");
        return RespBean.success("删除成功!");
    }

    @PostMapping("/updateAdmit/{student_number}/{admit}")
    public RespBean updateAdmit(@PathVariable("student_number") final String student_number, @PathVariable("admit") final boolean admit) {
        final int i = this.registrationService.updateAdmitByStudentNum(student_number, admit);
        if (i < 0)
            return RespBean.error("提交失败");
        return RespBean.success("提交成功!");
    }

    @PostMapping("/counts")
    public int counts() {
        return this.registrationService.counts();
    }

    @PostMapping("/search/{student_number}")
    public List<RegistrationDTO> search(@PathVariable("student_number") final String student_number) {
        return this.registrationService.getRegistrationMessageByNum(student_number);
    }


}
