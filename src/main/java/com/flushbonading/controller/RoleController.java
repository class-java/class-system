package com.flushbonading.controller;


import com.flushbonading.annotation.BackStage;
import com.flushbonading.pojo.RespBean;
import com.flushbonading.pojo.dto.RoleDTO;
import com.flushbonading.service.IRoleMenuService;
import com.flushbonading.service.IRoleService;
import com.flushbonading.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
@RestController
@RequestMapping("/role")
@Api(tags = "角色接口")
@BackStage
public class RoleController {
    @Autowired
    private IRoleService roleService;
    @Autowired
    private IRoleMenuService roleMenuService;
    @Autowired
    private IUserService userService;

    @PostMapping("/all")
    public List<RoleDTO> getAllRoleMessage() {
        return this.roleService.getAllRoleMessage();
    }

    @PostMapping("/edit/{role_id}/{row_name}/{have_permission}")
    public RespBean editRole(@PathVariable("role_id") final Integer role_id, @PathVariable("row_name") final String role_name, @PathVariable("have_permission") final String have_permission, @RequestBody final List<Integer> menus) {
        final RoleDTO roleDTO = RoleDTO.builder()
                .role_id(role_id)
                .role_name(role_name)
                .have_permission(have_permission)
                .authority(menus)
                .build();
        final int i = this.roleService.updateRole(roleDTO);
        menus.forEach(System.out::println);
        final int j = this.roleMenuService.insertRoleMenu(role_id, menus);
        if (i > 0 && j > 0)
            return RespBean.success("修改成功");
        return RespBean.error("修改失败");
    }

    //增加一个角色
    @PostMapping("/add/{role_name}/{have_permission}")
    public RespBean addRole(@PathVariable("role_name") final String role_name, @PathVariable("have_permission") final String have_permission, @RequestBody final List<Integer> menus) {
        final RoleDTO dto = RoleDTO.builder()
                .role_name(role_name)
                .have_permission(have_permission)
                .build();
        final int i = this.roleService.insertRole(dto);
        System.out.println(i);
        if (i > 0) {
            final int j = this.roleMenuService.insertRoleMenu(dto.getRole_id(), menus);
            System.out.println(j);
            if (j >= 0)
                return RespBean.success("角色创建成功");
        }
        System.out.println(dto.getRole_id());
        return RespBean.error("角色创建失败");
    }

    @PostMapping("/delete/{role_id}")
    public RespBean deleteARole(@PathVariable("role_id") final int role_id) {
        final int count = this.userService.selectCountByRoleID(role_id);
        if (count > 0) {
            return RespBean.error("删除失败:请删除该角色下的用户再删除该角色");
        }
        final int i = this.roleService.deleteRoleByID(role_id);
        if (i >= 0) {
            return RespBean.success("删除成功");
        }
        return RespBean.error("删除失败");
    }

    @PostMapping("/search/{role_id}/{role_name}")
    public List<RoleDTO> searchRole(@PathVariable("role_id") String role_id, @PathVariable("role_name") String role_name) {
        if (role_id.equals("@")) {
            role_id = "";
        }
        if (role_name.equals("@")) {
            role_name = "";
        }
        System.out.println(role_name);
        return this.roleService.searchRole(role_id, role_name);
    }

    @PostMapping("/allRole")
    @ApiOperation("获得所有角色名")
    public List<RoleDTO> getAllRoleName() {
        return this.roleService.getAllRoleName();
    }

}
