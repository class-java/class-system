package com.flushbonading.controller;


import com.flushbonading.annotation.BackStage;
import com.flushbonading.pojo.RespBean;
import com.flushbonading.pojo.dto.GradeDTO;
import com.flushbonading.service.IGradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zlx&zyx
 * @since 2021-04-07
 */
@RestController
@RequestMapping("/grade")
@BackStage
public class GradeController {
    @Autowired
    private IGradeService gradeService;

    @PostMapping("/all")
    public List<GradeDTO> getAll() {
        return this.gradeService.getList();
    }

    SimpleDateFormat sdf = new SimpleDateFormat("/yyyy/MM/dd/");

    @PostMapping("/import")
    public RespBean importData(final MultipartFile file, final HttpServletRequest req) throws IOException {
        final String format = this.sdf.format(new Date());
        final String realPath = req.getServletContext().getRealPath("/upload") + format;
        final File folder = new File(realPath);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        final String oldName = file.getOriginalFilename();
        final String newName = UUID.randomUUID().toString() + oldName.substring(oldName.lastIndexOf("."));
        try {
            file.transferTo(new File(folder, newName));
        } catch (final IOException e) {
            e.printStackTrace();
        }
        final String url = req.getScheme() + "://" + req.getServerName() + ":" + req.getServerPort() + "/upload" + format + newName;
        System.out.println(url);
        return RespBean.success("上传成功!");
    }
}
