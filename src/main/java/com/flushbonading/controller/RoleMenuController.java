package com.flushbonading.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
@RestController
@RequestMapping("//role-menu")
public class RoleMenuController {

}
