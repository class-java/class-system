package com.flushbonading.controller;

import com.flushbonading.annotation.BackStage;
import com.flushbonading.pojo.CourseType;
import com.flushbonading.service.ICourseTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/10 11:10
 * @since 1.0.0
 */
@RestController
@RequestMapping("/courseType")
@BackStage
public class CourseTypeController {
    @Autowired
    private ICourseTypeService courseTypeService;
    
    @PostMapping("/all")
    public List<CourseType> getAll() {
        return courseTypeService.getAll();
    }
}
