package com.flushbonading.controller;


import cn.hutool.core.lang.Assert;
import com.flushbonading.annotation.BackStage;
import com.flushbonading.pojo.RespBean;
import com.flushbonading.pojo.dto.CourseDTO;
import com.flushbonading.pojo.dto.CourseMessageDTO;
import com.flushbonading.service.ICourseService;
import com.flushbonading.service.IScoreService;
import com.flushbonading.util.DirectionUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 课程相关的前端控制器
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zlx&zyx
 * @since 2021-04-07
 */
@RestController
@RequestMapping("/course")
@Api(tags = "课程接口")
@BackStage
public class CourseController {
    /**
     * 自动注入课程服务对象
     */
    @Autowired
    private ICourseService courseService;
    /**
     * 自动注入成绩服务对象
     */
    @Autowired
    private IScoreService scoreService;

    /**
     * 返回课程详细信息
     * 路径名  <a>localhost:8088/course/all</a>
     *
     * @return 放回<code>CourseMessageDTO</code>对象
     * @see com.flushbonading.pojo.dto.CourseMessageDTO
     * @see com.flushbonading.service.impl.CourseServiceImpl
     */
    @ApiOperation("得到所有课程消息信息")
    @PostMapping("/all")
    public List<CourseMessageDTO> getAllMessage() {
        return this.courseService.getAllMessage();
    }

    /**
     * 通过学习方向名获得课程详细信息
     * 路径名  <a>localhost:8088/course/direction/{study_direction_name}</a>
     *
     * @param study_direction_name 从url携带进来的学习方向名
     * @return 返回课程消息信息
     * @throws IllegalArgumentException 传来的课程名不能为空
     */
    @ApiOperation("通过学习方向名取得该学习方向下课程的详细信息")
    @PostMapping("/direction/{study_direction_name}")
    public List<CourseMessageDTO> getCourseByStudyDirection(@PathVariable("study_direction_name") final String study_direction_name) {
        //利用断言判别空串
        Assert.notEmpty(study_direction_name, "传来的课程名不能为空");
        return this.courseService.getCourseByStudyDirection(DirectionUtil.directionID(study_direction_name));
    }

    /**
     * @param course_name     课程名
     * @param teacherName     教师名
     * @param type
     * @param scorePercentage 成绩占比
     * @return
     */
    @ApiOperation("插入课程接口")
    @PostMapping("/insert/{course_name}/{teacherName}/{type}/{scorePercentage}")
    public RespBean insertACourse(@PathVariable("course_name") final String course_name, @PathVariable("teacherName") final String teacherName, @PathVariable("type") final String type, @PathVariable("scorePercentage") final Double scorePercentage) {
        if (this.courseService.counts(course_name) != null) {
            return RespBean.error("该课程已经存在");
        }
        final int index = this.courseService.getLastCourseID();
        final int i = this.courseService.insertACourse(index + 1, course_name, teacherName, type, scorePercentage);
        final int j = this.scoreService.insertCourse(index + 1, type);
        //插入数据到score表中
        if (i > 0 && j > 0) {
            return RespBean.success("添加成功");
        }
        return RespBean.error("插入失败!");
    }

    @ApiOperation("通过课程id删除课程")
    @PostMapping("/delete/{course_id}")
    public RespBean deleteByCourseID(@PathVariable("course_id") final int course_id) {
        final int j = this.scoreService.deleteScoreByID(course_id);
        final int i = this.courseService.deleteByCourseID(course_id);
        System.out.println(i);
        if (i > 0 && j > 0) {
            return RespBean.success("删除成功");
        }
        return RespBean.error("删除失败");
    }

    @ApiOperation("课程管理子界面获得通过课程id查看具体课程信息")
    @PostMapping("/details/{course_id}")
    public CourseDTO getDetailsByID(@PathVariable("course_id") final int course_id) {
        return this.courseService.getDetailsByID(course_id);
    }

    @ApiOperation("课程管理子界面获得通过课程id更新具体课程信息")
    @PostMapping("/update/details/{course_id}")
    public RespBean updateDetailsByID(@PathVariable("course_id") final int course_id, @RequestBody final CourseDTO courseDTO) {
        final int i = this.courseService.updateDetailsByID(course_id, courseDTO);
        if (i > 0) {
            return RespBean.success("信息保存成功");
        }
        return RespBean.error("信息保存失败");
    }
}
