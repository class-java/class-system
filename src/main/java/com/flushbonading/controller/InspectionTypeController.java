package com.flushbonading.controller;

import com.flushbonading.annotation.BackStage;
import com.flushbonading.pojo.InspectionType;
import com.flushbonading.service.IInspectionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/10 11:11
 * @since 1.0.0
 */
@RestController
@RequestMapping("/inspectionType")
@BackStage
public class InspectionTypeController {
    @Autowired
    private IInspectionTypeService inspectionTypeService;
    @PostMapping("/all")
    public List<InspectionType> getAll(){
        return inspectionTypeService.getAll();
    }
}
