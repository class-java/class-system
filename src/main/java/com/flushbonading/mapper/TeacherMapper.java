package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.Teacher;
import com.flushbonading.pojo.dto.TeacherDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zlx
 * @author zk
 * @date 2021-04-07
 * @date 2021-08-30
 */
public interface TeacherMapper extends BaseMapper<Teacher> {
    /**
     * 通过学工号查询教师姓名
     *
     * @param teacher_id 学工号
     * @return 教师姓名
     */
    String getNameByTeacherId(@Param("teacher_id") String teacher_id);

    /**
     * 获得所有教师的姓名与序号
     *
     * @return 所有教师的姓名与序号的集合
     */
    List<TeacherDTO> getAllTeacher();

    /**
     * 通过teacher_name查询教师id序号
     *
     * @param teacher_name 教师姓名
     * @return 教师id序号
     */
    int getIdByTeacherName(@Param("teacher_name") String teacher_name);
}
