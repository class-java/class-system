package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.StudyDirection;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zlx
 * @author zk
 * @date 2021-04-07
 * @date 2021-08-30
 */
public interface StudyDirectionMapper extends BaseMapper<StudyDirection> {
    /**
     * 学习方向id查询查询学习方向
     *
     * @param study_direction_id 学习方向id
     * @return 学习方向
     */
    String selectStudyDirection(@Param("study_direction_id") Integer study_direction_id);
}
