package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.InspectionType;

import java.util.List;

/**
 * @author zyx 2134208960@qq.com
 * @date zk
 * @date 2021/5/10 10:41
 * @date 2021/08/27
 * @since 1.0.0
 */
public interface InspectionTypeMapper extends BaseMapper<InspectionType> {
    /**
     * 获得所有的考核类型id，考核类型
     *
     * @return 信息集合
     */
    List<InspectionType> getAll();
}
