package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.Project;
import com.flushbonading.pojo.dto.ProjectDTO;
import com.flushbonading.pojo.dto.ProjectListDAO;
import com.flushbonading.pojo.dto.ProjectStudentDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zlx
 * @author zk
 * @since 2021-04-07
 * @since 2021-08-27
 */
public interface ProjectMapper extends BaseMapper<Project> {
    /**
     * 获取最后一个项目id
     *
     * @return 后一个项目id
     */
    int getLastProjectID();

    /**
     * 获得所有的项目id,项目名,项目简介
     *
     * @return 项目id, 项目名, 项目简介集合
     */
    List<ProjectDTO> selectAll();

    /**
     * 添加项目（项目id，项目名，项目简介）
     *
     * @return 返回一个判断是否添加成功的值，成功大于0
     */
    int insertAProject(@Param("projectDTO") ProjectDTO projectDTO);

    /**
     * 通过项目id删除该项目
     *
     * @param project_id 项目id
     * @return 返回一个判断是否删除成功的值，成功大于0
     */
    int deleteProject(@Param("project_id") int project_id);

    /**
     * 通过项目id更改组名
     *
     * @param project_id 项目id
     * @param group_name 组名
     * @return 返回一个判断是否修改成功的值，成功大于0
     */
    int updateGroupName(@Param("project_id") int project_id, @Param("group_name") String group_name);

    /**
     * 通过项目id获取组名，学号，姓名，工作方向，项目职位
     *
     * @param project_id 项目id
     * @return 组名，学号，姓名，工作方向，项目职位信息集合
     */
    List<ProjectStudentDTO> selectStudentByID(@Param("project_id") int project_id);

    /**
     * 通过学号将t_student中的project_id修改为null
     *
     * @param student_number 学号
     * @return 返回一个判断是否修改成功的值，成功大于0
     */
    int deleteAStudent(@Param("student_number") String student_number);

    /**
     * 通过项目id修改项目信息
     *
     * @param project_id 项目id
     * @param projectDTO 项目信息
     * @return 返回一个判断是否修改成功的值，成功大于0
     */
    int updateProject(@Param("project_id") int project_id, @Param("projectDTO") ProjectDTO projectDTO);

    /**
     * 通过学号查询项目简介
     *
     * @param student_number 学号
     * @return 项目简介
     */
    String selectIntroduction(@Param("student_number") String student_number);

    /**
     * 通过项目id，工作方向id更新信息
     *
     * @param projectStudentDTO 学生项目信息
     * @param project_id        项目id
     * @param operating_post_id 工作方向id
     * @return 返回一个判断是否修改成功的值，成功大于0
     */
    int updateStudent(@Param("projectStudentDTO") ProjectStudentDTO projectStudentDTO, @Param("project_id") int project_id, @Param("operating_post_id") int operating_post_id);

    /**
     * 获取所有的项目id和项目名
     *
     * @return 项目id和项目名集合
     */
    List<ProjectListDAO> selectAllProjects();
}
