package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.Registration;
import com.flushbonading.pojo.dto.FileRegistrationDTO;
import com.flushbonading.pojo.dto.RegistrationDTO;
import com.flushbonading.pojo.dto.RegistrationTempDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zlx
 * @author zk
 * @date 2021-04-07
 * @date 2021-08-27
 */
@Mapper
public interface RegistrationMapper extends BaseMapper<Registration> {
    /**
     * 通过学号查询学生姓名
     *
     * @param student_number 学号
     * @return 学生姓名
     */
    String getNameByStudentNum(@Param("student_number") String student_number);

    /**
     * 获得t_registration表中的所有学生信息
     *
     * @return 学生信息集合
     */
    List<RegistrationDTO> getRegistrationMessage();

    /**
     * 通过学号删除t_registration表中对应的学生信息
     *
     * @param student_number 学号
     * @return 返回一个判断是否删除成功的值，成功大于0
     */
    int deleteRegistrationByStudentNum(@Param("student_number") String student_number);

    /**
     * 通过学号修改是否被录取信息
     *
     * @param student_number 学号
     * @param admit          是否被录取
     * @return 返回一个判断是否修改成功的值，成功大于0
     */
    int updateAdmitByStudentNum(@Param("student_number") String student_number, @Param("admit") boolean admit);

    /**
     * 查询t_registration表中的学生信息个数
     *
     * @return 学生信息个数
     */
    int counts();

    /**
     * 通过学号获得学生信息
     *
     * @param student_number 学号
     * @return 学生信息集合
     */
    List<RegistrationDTO> getRegistrationMessageByNum(@Param("student_number") String student_number);

    /**
     * 添加报名人员信息
     *
     * @param registrationTempDTO 报名人员信息
     * @return 返回一个判断是否添加成功的值，成功大于0
     */
    Integer insertRegistration(@Param("registrationTempDTO") RegistrationTempDTO registrationTempDTO);


    /**
     * 通过学号查找报名信息个数
     *
     * @param student_number 学号
     * @return 信息个数
     */
    Integer registrationCounts(@Param("student_number") String student_number);

    /**
     * 重复提交报名信息，即根据学号更新信息
     *
     * @param registrationTempDTO 报名信息
     * @return 返回一个判断是否修改成功的值，成功大于0
     */
    Integer updateRegistration(@Param("registrationTempDTO") RegistrationTempDTO registrationTempDTO);

    /**
     * 检验身份证的唯一性
     *
     * @param id_card
     * @return
     */
    Integer getCountByIdCard(@Param("id_card") String id_card);

    /**
     * 获得t_registration表中的所有学生信息
     *
     * @return 学生信息集合
     */
    List<RegistrationTempDTO> getRegistrationTempDto();

    /**
     * 获得t_registration表中对应的FileRegistrationDTO所有学生信息
     * 用于导出excel文件
     *
     * @return 学生信息集合
     */
    List<FileRegistrationDTO> getFileRegistrationDto();

    /**
     * 录取报名学生，admit==2时录取
     *
     * @param registrationTempDTO 学生信息
     * @return 返回一个判断是否修改成功的值，成功等于0
     */
    Integer updateRegistrationAdmit(@Param("registrationTempDTO") List<RegistrationTempDTO> registrationTempDTO);

    /**
     * 将通过录取的同学添加到学生表
     *
     * @param registrationTempDTO 报名信息
     * @return 返回一个判断是否添加成功的值，成功等于0
     */
    Integer insertAtStudent(@Param("registrationTempDTO") RegistrationTempDTO registrationTempDTO);

    /**
     * 通过学号修改报名信息
     *
     * @param registrationTempDTO 学生信息
     * @return 返回一个判断是否修改成功的值，成功大于0
     */
    Integer updateRegistrationData(@Param("registrationTempDTO") RegistrationTempDTO registrationTempDTO);
}
