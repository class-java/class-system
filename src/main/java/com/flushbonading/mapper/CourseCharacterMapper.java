package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.CourseCharacter;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/10 10:42
 * @since 1.0.0
 */
public interface CourseCharacterMapper extends BaseMapper<CourseCharacter> {
}
