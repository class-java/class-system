package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.Role;
import com.flushbonading.pojo.dto.RoleDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zlx&zyx
 * @author zk
 * @date 2021-04-07
 * @date 2021-08-30
 */
public interface RoleMapper extends BaseMapper<Role> {
    /**
     * 通过角色id查看角色名
     *
     * @param role_id 角色id
     * @return 角色名
     */
    String getNameByRoleId(@Param("role_id") int role_id);

    /**
     * 获得角色id，角色名以及角色权限
     *
     * @return RoleDTO
     */
    List<RoleDTO> getAllRoleMessage();

    /**
     * 根据角色id修改角色名与角色权限
     *
     * @param roleDTO 信息
     * @return 返回一个判断是否修改成功的值，成功大于0
     */
    int updateRole(@Param("roleDTO") RoleDTO roleDTO);

    /**
     * 降序排序获得第一个角色
     *
     * @return 角色id
     */
    int getLastID();

    /**
     * 添加角色id，角色名，角色权限信息
     *
     * @param roleDTO 所需对象信息
     * @return 返回一个判断是否添加成功的值，成功大于0
     */
    int insertRole(@Param("roleDTO") RoleDTO roleDTO);

    /**
     * 删除角色id对应的信息
     *
     * @param role_id 角色id
     * @return 返回一个判断是否删除成功的值，成功大于0
     */
    int deleteRoleByID(@Param("role_id") int role_id);

    /**
     * 通过角色id与角色名获取信息
     *
     * @param role_id   角色id
     * @param role_name 角色名
     * @return RoleDTO信息
     */
    List<RoleDTO> searchRole(@Param("role_id") String role_id, @Param("role_name") String role_name);

    /**
     * 获取所有的角色名
     *
     * @return RoleDTO所有的角色名
     */
    List<RoleDTO> getAllRoleName();
}
