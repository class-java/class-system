package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.User;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zlx
 * @author zk
 * @date 2021-04-08
 * @date 2021-08-24
 */
public interface UserMapper extends BaseMapper<User> {
    /**
     * 通过id获取用户信息
     *
     * @param userId id
     * @return 用户信息
     */
    User getUserId(@Param("userId") String userId);

    /**
     * 更新用户信息
     *
     * @param user 用户信息
     */
    void updateUser(@Param("user") User user);

    /**
     * 同user_id查询用户权限
     *
     * @param user_id id
     * @return 用户权限
     */
    User getRoleByUserId(@Param("user_id") String user_id);

    /**
     * 查询role_id权限的人数
     *
     * @param role_id id
     * @return 权限的人数
     */
    int selectCountByRoleID(@Param("role_id") int role_id);

    /**
     * 通过userId删除用户信息
     *
     * @param userId id
     * @return 返回一个判断是否删除成功的值，成功大于0
     */
    int deleteAUser(@Param("user_id") String user_id);


    /**
     * 通过user_id和role_name添加新用户
     *
     * @param user_id   id
     * @param role_name 姓名
     * @return 返回一个判断是否添加成功的值，成功大于0
     */
    int insertAUser(@Param("user_id") String user_id, @Param("role_name") String role_name);

}
