package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.Grade;
import com.flushbonading.pojo.dto.GradeDTO;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zlx&zyx
 * @author zk
 * @date 2021-04-07
 * @date 2021-08-27
 */
public interface GradeMapper extends BaseMapper<Grade> {
    /**
     * 获得所有的年级id与年级名
     *
     * @return 信息集合
     */
    List<GradeDTO> getList();
}
