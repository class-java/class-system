package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.Course;
import com.flushbonading.pojo.dto.CourseDTO;
import com.flushbonading.pojo.dto.CourseMessageDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zlx
 * @author zk
 * @date 2021-04-07
 * @date 2021-08-30
 */
public interface CourseMapper extends BaseMapper<Course> {

    /**
     * 获得所有的课程信息
     *
     * @return 课程信息集合
     */
    List<CourseMessageDTO> getAllMessage();

    /**
     * 通过学习方向id获取课程信息
     *
     * @param study_direction_id 学习方向id
     * @return 课程信息集合
     */
    List<CourseMessageDTO> getCourseByStudyDirection(@Param("study_direction_id") int study_direction_id);

    /**
     * 获得最后一条课程id
     *
     * @return 课程id
     */
    int getLastCourseID();

    /**
     * 添加课程信息
     *
     * @param courseMessageDTO 课程信息对象
     * @return 返回一个判断是否添加成功的值，成功大于0
     */
    int insertACourse(@Param("courseMessageDTO") CourseMessageDTO courseMessageDTO);

    /**
     * 通过课程id删除课程信息
     *
     * @param cidrse_id 课程id
     * @return 返回一个判断是否删除成功的值，成功大于0
     */
    int deleteByCourseID(@Param("course_id") int course_id);

    /**
     * 通过课程名获取课程id
     *
     * @param course_name 课程名
     * @return 课程id
     */
    int getCourseIDByName(@Param("course_name") String course_name);

    /**
     * 通过课程名查找课程数
     *
     * @param course_name 课程名
     * @return 课程数
     */
    Integer counts(@Param("course_name") String course_name);

    /**
     * 通过课程id查找课程信息
     *
     * @param course_id 课程id
     * @return CourseDTO课程信息
     */
    CourseDTO getDetailsByID(@Param("course_id") int course_id);

    /**
     * 通过课程id修改课程信息
     *
     * @param course_id          课程id
     * @param course_name        课程名
     * @param teacher_id         教师id
     * @param credit             学分
     * @param course_type_id     课程类型id
     * @param inspection_type_id 考察类型id
     * @param scorePercentage    考试占比
     * @return 返回一个判断是否修改成功的值，成功大于0
     */
    int updateDetailsByID(@Param("course_id") int course_id, @Param("course_name") String course_name, @Param("teacher_id") int teacher_id, @Param("credit") Double credit, @Param("course_type_id") int course_type_id, @Param("inspection_type_id") int inspection_type_id, @Param("scorePercentage") Double scorePercentage);
}
