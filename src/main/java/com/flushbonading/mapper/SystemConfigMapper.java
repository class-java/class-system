package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.SystemConfig;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zk
 * @since 2021-10-11
 */
public interface SystemConfigMapper extends BaseMapper<SystemConfig> {

    /**
     * 添加系统的配置信息
     *
     * @param systemConfig 配置信息
     * @return 返回一个判断是否添加成功的值，成功大于0
     */
    Integer insertConfigInformation(@Param("systemConfig") SystemConfig systemConfig);

    /**
     * 修改系统的配置信息
     *
     * @param systemConfig 配置信息
     * @return 返回一个判断是否添加成功的值，成功大于0
     */
    Integer updateConfigInformation(@Param("systemConfig") SystemConfig systemConfig);

    /**
     * 查找某个系统配置类型的个数
     *
     * @param type 配置类型
     * @return 个数
     */
    Integer configCounts(@Param("type") String type);

    /**
     * 根据配置类型查找配置信息
     *
     * @param type 配置类型
     * @return 配置信息
     */
    String configContent(@Param("type") String type);
}
