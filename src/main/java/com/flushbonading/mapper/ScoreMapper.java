package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.Score;
import com.flushbonading.pojo.dto.ScoreDTO;
import com.flushbonading.pojo.dto.ScoreMessageDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zlx
 * @author zk
 * @date 2021-04-07
 * @date 2021-08-30
 */
public interface ScoreMapper extends BaseMapper<Score> {
    /**
     * 更新成绩  总分=成绩*0.7+平时分*0.3
     *
     * @return 返回一个判断更新是否成功的值，成功大于0
     */
    int updateAllScore();

    /**
     * 通过课程id查询所有学生成绩
     *
     * @param course_id 课程id
     * @return 所有学生成绩
     */
    List<ScoreDTO> getSocresByCourseID(@Param("course_id") int course_id);

    /**
     * 通过课程id来更新学生成绩信息
     *
     * @param course_id 课程id
     * @param scoreDTO  学生信息
     * @return 返回一个判断更新是否成功的值，成功大于0
     */
    int updateCourses(@Param("course_id") int course_id, @Param("scoreDTO") ScoreDTO scoreDTO);

    /**
     * 通过课程id与学生信息来更新总成绩
     *
     * @param course_id 课程id
     * @param scoreDTO  学生信息
     * @return 返回一个判断更新是否成功的值，成功大于0
     */
    int updateTotalScore(@Param("course_id") int course_id, @Param("scoreDTO") ScoreDTO scoreDTO);

    /**
     * 通过课程id和学号来删除该学生成绩信息
     *
     * @param course_id      课程id
     * @param student_number 学号
     * @return 返回一个判断删除是否成功的值，成功大于0
     */
    int deleteAScore(@Param("course_id") int course_id, @Param("student_number") String student_number);

    /**
     * 根据方向向score插入数据
     *
     * @param course_id          课程id
     * @param study_direction_id 学习方向
     * @return 返回一个判断插入是否成功的值，成功大于0
     */
    int insertCourse(@Param("course_id") int course_id, @Param("study_direction_id") int study_direction_id);

    /**
     * 通过课程id删除全部成绩
     *
     * @param course_id 课程id
     * @return 返回一个判断删除是否成功的值，成功大于0
     */
    int deleteCourse(@Param("course_id") int course_id);

    /**
     * 通过课程id,学号,姓名查询总成绩
     *
     * @param course_id      课程id
     * @param student_number 学号
     * @param name           姓名
     * @return 学生成绩信息
     */
    List<ScoreDTO> searchScore(@Param("course_id") int course_id, @Param("student_number") String student_number, @Param("name") String name);

    /**
     * 通过学号查询学生成绩ScoreMessageDTO类信息
     *
     * @param student_number 学号
     * @return 学生成绩ScoreMessageDTO类信息
     */
    List<ScoreMessageDTO> getScoresByStudentNumber(@Param("student_number") String student_number);
}
