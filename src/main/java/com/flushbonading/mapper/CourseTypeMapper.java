package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.CourseType;

import java.util.List;

/**
 * @author zyx 2134208960@qq.com
 * @author zk
 * @date 2021/5/10 10:39
 * @date 2021/08/27
 * @since 1.0.0
 */
public interface CourseTypeMapper extends BaseMapper<CourseType> {
    /**
     * 获得所有的课程id与课程类型名
     *
     * @return 信息集合
     */
    List<CourseType> getAll();
}
