package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.Status;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/18 20:50
 * @since 1.0.0
 */
public interface StatusMapper extends BaseMapper<Status> {
}
