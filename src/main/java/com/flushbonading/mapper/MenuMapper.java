package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.Menu;
import com.flushbonading.pojo.dto.MenuDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zlx
 * @author zk
 * @date 2021-04-07
 * @date 2021-08-27
 */
public interface MenuMapper extends BaseMapper<Menu> {
    /**
     * 通过用户id获得菜单
     *
     * @param user_id
     * @return 菜单信息集合
     */
    List<Menu> getMenu(@Param("user_id") String user_id);

    /**
     * 通过parentId获得菜单子列表
     *
     * @param parent_id
     * @return 菜单子列表信息集合
     */
    List<Menu> getAllByParentId(@Param("parent_id") Integer parent_id);

    /**
     * 通过role_id获得菜单列表
     *
     * @param role_id
     * @return 菜单列表信息集合
     */
    List<Menu> getRoleMenu(@Param("role_id") Integer role_id);

    /**
     * 获得菜单id ,菜单名,父菜单id
     *
     * @return 信息集合
     */
    List<MenuDTO> getAllPermission();

    /**
     * 通过角色id查找单id ,菜单名,父菜单
     *
     * @param role_id 角色id
     * @return 信息集合
     */
    List<MenuDTO> selectMenuByRoleID(@Param("role_id") int role_id);
}
