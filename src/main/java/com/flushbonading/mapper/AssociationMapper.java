package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.Association;
import com.flushbonading.pojo.dto.AssociationDTO;
import org.apache.ibatis.annotations.Param;

/**
 * @author zyx 2134208960@qq.com
 * @author zk
 * @date 2021/5/18 20:41
 * @date 2021/08/30
 * @since 1.0.0
 */
public interface AssociationMapper extends BaseMapper<Association> {
    /**
     * 插入一个学生到协会
     *
     * @param associationDTO 从服务层携带的对象
     * @return 返回修改的数值
     */
    Integer insertAStudent(@Param("associationDTO") AssociationDTO associationDTO);

    /**
     * 通过学生学号返回该学号在表中的数量
     *
     * @param student_number 学生学号
     * @return 返回int值
     */
    int getCountsByStudentNumber(@Param("student_number") String student_number);
}
