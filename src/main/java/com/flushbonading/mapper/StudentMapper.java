package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.Student;
import com.flushbonading.pojo.dto.StudentDTO;
import com.flushbonading.pojo.dto.StudentMessageDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zlx&zyx
 * @since 2021-04-07
 */
public interface StudentMapper extends BaseMapper<Student> {
    /**
     * 通过学号获得学生信息
     *
     * @param student_number 学号
     * @return 学生信息Student
     */
    Student getStudent(@Param("student_number") String student_number);

    /**
     * 通过学号获得姓名
     *
     * @param student_number 学号
     * @return 学生姓名
     */
    String getNameByStudentNum(@Param("student_number") String student_number);

    /**
     * 通过学号获得学生信息
     *
     * @param student_number 学生学号
     * @return 学生信息StudentDTO
     */
    StudentDTO selectStudentMessage(@Param("student_number") String student_number);

    /**
     * 通过学生学号来修改学生信息
     *
     * @param student_number 学生的学号
     * @param studentDTO     新的学生信息
     * @return 返回一个判断是否修改成功的值，成功大于0
     */
    int updateStudentMessage(@Param("studentDTO") StudentDTO studentDTO, @Param("student_number") String student_number);

    /**
     * 修改项目id,赋值为null
     *
     * @param project_id 项目id
     * @return 返回一个判断是否修改成功的值，成功大于0
     */
    int updateProjectID(@Param("project_id") int project_id);

    /**
     * 通过学号修改项目id,岗位编号,岗位
     *
     * @param student_number      学号
     * @param project_id          项目id
     * @param operating_post_id   岗位编号
     * @param technical_direction 岗位
     * @return 返回一个判断是否修改成功的值，成功大于0
     */
    int updateProject(@Param("student_number") String student_number, @Param("project_id") int project_id, @Param("operating_post_id") int operating_post_id, @Param("technical_direction") String technical_direction);

    /**
     * 通过学号查找学生有多少个项目
     *
     * @param student_number 学号
     * @return 项目个数
     */
    int atTheProjectCounts(@Param("student_number") String student_number);

    /**
     * 通过学号查找项目id
     *
     * @param student_number 学号
     * @return 项目id
     */
    Integer getProjectIDByStudentNumber(@Param("student_number") String student_number);

    /**
     * 通过学号来获得名字
     *
     * @param student_number 学号
     * @return 名字
     */
    String getName(@Param("student_number") String student_number);

    /**
     * 通过多表查询获得全部StudentMessageDTO信息
     *
     * @return 全部StudentMessageDTO信息
     */
    List<StudentMessageDTO> getAllStudent();

    /**
     * 通过学号删除学生
     *
     * @param student_number 学号
     * @return 返回一个判断是否删除成功的值，成功大于0
     */
    int deleteAStudent(@Param("student_number") String student_number);

    /**
     * 通过传入StudentMessageDTO来修改学生信息
     *
     * @param studentMessageDTO 学生信息
     * @return 返回一个判断是否修改成功的值，成功大于0
     */
    int updateStudentMessageByStudentNumber(@Param("studentMessageDTO") StudentMessageDTO studentMessageDTO);

    /**
     * 添加学生信息
     *
     * @param grade             年级
     * @param studentMessageDTO 学生信息
     * @return 返回一个判断是否添加成功的值，成功大于0
     */
    int insertAStudent(@Param("grade") int grade, @Param("studentMessageDTO") StudentMessageDTO studentMessageDTO);

    /**
     * 通过名字来查询学生信息
     *
     * @param name 学生姓名
     * @return 学生信息
     */
    List<StudentMessageDTO> searchByName(@Param("name") String name);
}
