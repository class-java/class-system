package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.RoleMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zlx
 * @author zk
 * @date 2021-04-07
 * @date 2021-08-30
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
    /**
     * 通过角色id删除所有对应的角色
     *
     * @param role_id 角色
     * @return 返回一个判断是否删除成功的值，成功大于0
     */
    int deleteRoleMenuByID(@Param("role_id") int role_id);

    /**
     * 添加角色和菜单id
     *
     * @param role_id 角色
     * @param menus   菜单id
     * @return 返回一个判断是否添加成功的值，成功大于0
     */
    int insertRoleMenu(@Param("role_id") int role_id, @Param("menus") List<Integer> menus);
}
