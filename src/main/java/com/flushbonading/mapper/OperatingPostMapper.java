package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.OperatingPost;
import org.apache.ibatis.annotations.Param;

/**
 * @author zyx 2134208960@qq.com
 * @author zk
 * @date 2021/5/6 17:01
 * @date 2021/8/27
 * @since 1.0.0
 */
public interface OperatingPostMapper extends BaseMapper<OperatingPost> {
    /**
     * 通过项目职位名获取id
     *
     * @param operating_post_name 项目职位
     * @return id
     */
    int getIDByName(@Param("operating_post_name") String operating_post_name);
}
