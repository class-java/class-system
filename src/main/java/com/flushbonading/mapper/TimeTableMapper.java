package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.TimeTable;
import org.apache.ibatis.annotations.Param;

/**
 * @author zyx 2134208960@qq.com
 * @author zk
 * @date 2021/5/4 17:15
 * @date 2021/08/30
 * @since 1.0.0
 */
public interface TimeTableMapper extends BaseMapper<TimeTable> {
    /**
     * 通过修改path路径，来修改课程表
     *
     * @param path
     * @return 返回一个判断是否修改成功的值，成功大于0
     */
    int updatePath(@Param("path") String path);

    /**
     * 查询课表
     *
     * @return 地址
     **/
    String selectPath();
}
