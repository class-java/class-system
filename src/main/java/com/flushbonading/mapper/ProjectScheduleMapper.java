package com.flushbonading.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flushbonading.pojo.ProjectSchedule;
import com.flushbonading.pojo.dto.ScheduleDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zyx 2134208960@qq.com
 * @author zk
 * @date 2021/5/12 9:48
 * @date 2021/08/30
 * @since 1.0.0
 */
public interface ProjectScheduleMapper extends BaseMapper<ProjectSchedule> {

    /**
     * 添加项目进度信息
     *
     * @param scheduleDTO 项目进度信息
     * @return 返回一个判断是否添加成功的值，成功大于0
     */
    int insertSchedule(@Param("scheduleDTO") ScheduleDTO scheduleDTO);

    /**
     * 通过项目id查找项目进度信息
     *
     * @param project_id 项目id
     * @return 项目进度信息集合
     */
    List<ScheduleDTO> getAllSchedule(@Param("project_id") int project_id);

    /**
     * 通过学号查找项目进度信息
     *
     * @param student_number 学号
     * @return 项目进度信息集合
     */
    List<ScheduleDTO> getAllScheduleByStudentNumber(@Param("student_number") String student_number);

    /**
     * 删除项目进度信息
     *
     * @param scheduleDTO 要删除的项目进度信息
     * @return 返回一个判断是否删除成功的值，成功大于0
     */
    int deleteASchedule(@Param("scheduleDTO") ScheduleDTO scheduleDTO);
}
