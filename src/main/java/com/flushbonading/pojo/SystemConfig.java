package com.flushbonading.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zk
 * @since 2021-10-11
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    @TableName("t_system_config")
public class SystemConfig implements Serializable {

    private static final long serialVersionUID=1L;

      /**
     * id
     */
        @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

      /**
     * 配置类型(功能)
     */
      private String type;

      /**
     * 配置信息
     */
      private String content;

      /**
     * 备注
     */
      private String remark;


}
