package com.flushbonading.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@TableName("t_registration")
@ApiModel(value = "Registration对象", description = "")
public class Registration implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "学号")
    private String student_number;

    @ApiModelProperty(value = "身份证号")
    private String id_card;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "性别")
    private Integer gender;

    @ApiModelProperty(value = "班级")
    private String class_name;

    @ApiModelProperty(value = "社会实践情况")
    private String experience;

    @ApiModelProperty(value = "学科竞赛情况")
    private String skill;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime update_time;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime create_time;

    @ApiModelProperty(value = "是否被录取")
    private Integer admit;

    @ApiModelProperty(value = "角色")
    private Integer role_id;

    @ApiModelProperty(value = "C语言成绩")
    private Integer c_language;

    @ApiModelProperty(value = "数字电路成绩")
    private Integer circuit;

    @ApiModelProperty(value = "政治面貌:0、群众 1、共青团员 2、中共预备党员 3、中共党员")
    private Integer politics_status;

    @ApiModelProperty(value = "班级内职务")
    private String duty;

    @ApiModelProperty(value = "奖惩情况")
    private String rewards_punishment;

    @ApiModelProperty(value = "挂科情况")
    private String fail_exam;

    @ApiModelProperty(value = "自我评价")
    private String remark;

    @ApiModelProperty(value = "学习方向，提供两个选择：0、嵌入式系统 3、应用端软件")
    private Integer study_direction;
}
