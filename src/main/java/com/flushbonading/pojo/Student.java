package com.flushbonading.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_student")
@ApiModel(value = "Student对象", description = "")
@Builder
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "年级id")
    private Integer grade_id;

    @ApiModelProperty(value = "学号")
    private String student_number;

    @ApiModelProperty(value = "身份证号")
    private String id_card;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "性别")
    private String gender;

    @ApiModelProperty(value = "班级")
    private String class_name;

    @ApiModelProperty(value = "项目经验")
    private String experience;

    @ApiModelProperty(value = "技能与获奖")
    private String skill;

    @ApiModelProperty(value = "系别")
    private String department;

    @ApiModelProperty(value = "学习方向id")
    private Integer study_direction_id;

    @ApiModelProperty(value = "所选项目id")
    private Integer project_id;

    @ApiModelProperty(value = "政治面貌")
    private String politics_status;

    @ApiModelProperty(value = "学生工作职务")
    private String student_job;

    @ApiModelProperty(value = "联系电话")
    private String phone;

    @ApiModelProperty(value = "qq号")
    private String qq;

    @ApiModelProperty(value = "一卡通号")
    private String student_card_number;

    @ApiModelProperty(value = "银行卡号")
    private String bank_card;

    @ApiModelProperty(value = "家庭住址")
    private String address;

    @ApiModelProperty(value = "邮政编码")
    private String postal_code;

    @ApiModelProperty(value = "家长姓名")
    private String parent_name;

    @ApiModelProperty(value = "家长电话")
    private String parent_phone;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime update_time;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime create_time;

    @ApiModelProperty(value = "学习方向")
    @TableField(exist = false)
    private String study_direction;

    @ApiModelProperty(value = "项目组")
    @TableField(exist = false)
    private String project;

    @ApiModelProperty(value = "民族")
    private String nationality;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "岗位编号")
    private Integer operation_post_id;

    @ApiModelProperty(value = "技术方向")
    private String technical_direction;

}
