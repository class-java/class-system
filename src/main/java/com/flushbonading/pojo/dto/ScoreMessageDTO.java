package com.flushbonading.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/10 21:29
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ScoreMessageDTO {
    private String course_name; //课程名
    private Double credit;     //学分
    private String category;    //课程类别
    private String assessment_method;  //考核类型
    private String nature_of_study;     //修读类型
    private Double score;  //成绩
    private Double usually_score;//平时成绩
    private Double total_score;//总成绩
    private String remarks;
}
