package com.flushbonading.pojo.dto;

import lombok.*;

/**
 * @author zk
 * @date 2021/9/24 15:31
 */
@Data
@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationTempDTO {
    private String student_number;

    private String name;

    private Integer gender;

    private String id_card;

    private String class_name;

    private String duty;
    private Integer politics_status;
    private String experience;
    private String skill;
    private String rewards_punishment;
    private String fail_exam;
    private String remark;
    private String str_study_direction;
    private Integer study_direction;
    private Integer admit;
    private Integer c_language;
    private Integer circuit;
}
