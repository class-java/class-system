package com.flushbonading.pojo.dto;

import lombok.*;

/**
 * 课程详细信息
 * 利用Lombok插件快速构建
 * 可用构造者模式进行创建对象
 * @author zyx 2134208960@qq.com
 * @date 2021/4/20 10:50
 * @since 1.0.0
 */
@Data
@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CourseMessageDTO {
    /**
     * 课程id号
     */
    private Integer course_id;

    /**
     * 课程名
     */
    private String course_name;

    /**
     * 教师名(在教师表中的字段)
     */
    private String teacherName;

    /**
     * 学习方向id
     */
    private Integer study_direction_id;

    /**
     * 成绩占比
     */
    private Double scorePercentage;
}
