package com.flushbonading.pojo.dto;

import com.flushbonading.annotation.StudentNumber;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @Author zlx
 * @description
 * @Version 1.0
 * @Date 2021/4/8 21:17
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    @ApiModelProperty(value = "id,学生学号或者老师工号")
    @StudentNumber
    private String id;

    @ApiModelProperty(value = "密码")
    @NotBlank
    @Pattern(regexp = "^[A-Za-z0-9]+$", message = "密码不符合规范")
    @Length(min = 6)
    private String password;
}
