package com.flushbonading.pojo.dto;

import com.flushbonading.annotation.StudentNumber;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/4/23 15:59
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ScoreDTO {
    @StudentNumber
    private String student_number;
    private String name;
    private Double score;
    private Double usually_score;
    private Double total_score;
    private Double scorePercentage = 0.7;
    private Double usuallyPercentage = 1 - scorePercentage;
    private Boolean modifyGrade = false;
    private String Switch = "编辑";
    private String change = "删除";
}
