package com.flushbonading.pojo.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.*;

import java.io.Serializable;

/**
 * @author zk
 * @date 2021/9/28 13:01
 */
@Data
@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class FileRegistrationDTO implements Serializable {
    @ExcelProperty(value = "学号")
    private String student_number;

    @ExcelProperty(value = "姓名")
    private String name;

    @ExcelProperty(value = "性别(0:男 1:女孩)")
    private Integer gender;

    @ExcelProperty(value = "身份证")
    private String id_card;

    @ExcelProperty(value = "班级")
    private String class_name;

    @ExcelProperty(value = "班级内职务")
    private String duty;

    @ExcelProperty(value = "学习方向")
    private Integer study_direction;

    @ExcelProperty(value = "政治面貌")
    private Integer politics_status;

    @ExcelProperty(value = "社会实践情况")
    private String experience;

    @ExcelProperty(value = "学科竞赛情况")
    private String skill;

    @ExcelProperty(value = "奖惩情况")
    private String rewards_punishment;

    @ExcelProperty(value = "挂科情况")
    private String fail_exam;

    @ExcelProperty(value = "自我评价")
    private String remark;
}
