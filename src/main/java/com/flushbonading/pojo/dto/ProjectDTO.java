package com.flushbonading.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/4/29 15:00
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjectDTO {

    private int project_id;

    private String project_name;
    //项目简介
    private String project_Introduction;
}
