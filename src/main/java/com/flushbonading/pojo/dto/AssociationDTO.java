package com.flushbonading.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/18 20:55
 * @since 1.0.0
 */
@Data
@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value="AssociationDTO对象", description="前后端传输协会信息对象")
public class AssociationDTO {
    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "性别")
    private String resource;

    @ApiModelProperty(value = "生日")
    private String date;

    @ApiModelProperty(value = "年龄")
    private Integer age;

    @ApiModelProperty(value = "系部")
    private String department;

    @ApiModelProperty(value = "班级")
    private String class_name;

    @ApiModelProperty(value = "学号")
    private String num;

    @ApiModelProperty(value = "民族")
    private String nation;

    @ApiModelProperty(value = "籍贯")
    private String address;

    @ApiModelProperty(value = "qq")
    private String qq;

    @ApiModelProperty(value = "手机号")
    private String tle;

    @ApiModelProperty(value = "在协会的状态")
    private String status_name;
}
