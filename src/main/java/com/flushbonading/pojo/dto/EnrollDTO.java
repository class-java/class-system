package com.flushbonading.pojo.dto;

import com.flushbonading.annotation.StudentNumber;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author zyx 2134208960@qq.com
 * @version 0.1.0
 * @create 2021-03-13 09:53
 * @since 0.1.0
 **/
//代码生成器
//注册模块传输对象
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EnrollDTO {
    //班级
    @NotBlank
    private String className;

    //姓名
    @NotBlank
    @Pattern(regexp = "^[\u4e00-\u9fa5]{0,}$")   //检测汉字的pattern表达式
    private String name;

    //性别
    @NotNull
    @Pattern(regexp = "[/^男$|^女&/]")
    private Integer gender;

    //学号
    @StudentNumber
    private String studentNumber;

    //身份证
    @NotBlank
    @Pattern(regexp = "(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)", message = "请输入正确的身份证号")
    private String IdCard;

    //密码
    @NotBlank
    @Pattern(regexp = "^[A-Za-z0-9]+$", message = "密码不符合规范")
    @Length(min = 6)
    private String password;

    //项目经验
    @Length(max = 256)
    private String experiences;
    //技能与获奖
    @Length(max = 256)
    private String skill;
}
