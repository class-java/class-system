package com.flushbonading.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/4/29 14:52
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PerformanceDTO {
    private String course_name;
    private Double score;
    private Double usually_score;
    private Double total_score;
}
