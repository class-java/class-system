package com.flushbonading.pojo.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * 存放用户名和角色信息的实体类*
 *
 * @author zyx 2134208960@qq.com
 * @date 2021/4/12 20:01
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserMessageDTO {
    @NotBlank
    private String name;
    @NotBlank
    private String roleName;
}
