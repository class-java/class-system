package com.flushbonading.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/13 15:11
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentMessageDTO {
    private String student_number;
    private String student_name;
    private String class_name;
    private String sex;
    private String direction;
    private String project_group;
    private String job_title;
    private String role_name;
}
