package com.flushbonading.pojo.dto;

import com.flushbonading.annotation.StudentNumber;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author zyx 2134208960@qq.com
 * @version 0.1.0
 * @create 2021-03-13 11:14
 * @since 0.1.0
 **/
//登入模块传输对象
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginDTO {
    //学号
    @StudentNumber
    private String studentNumber;

    //密码
    @NotBlank
    @Pattern(regexp = "^[A-Za-z0-9]+$",message = "密码不符合规范")
    @Length(min = 6)
    private String password;
}
