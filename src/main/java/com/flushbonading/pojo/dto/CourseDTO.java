package com.flushbonading.pojo.dto;

import lombok.*;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/10 19:33
 * @since 1.0.0
 */
@Data
@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CourseDTO {
    private String course_name;
    private String teacher_name;
    private Double credit;
    //课程类型
    private String category;
    //考核方式
    private String assessment_method;

    //成绩占比
    private Double exam_score;

    //平时成绩
    private Double usually_score;
}
