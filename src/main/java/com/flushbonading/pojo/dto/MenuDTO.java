package com.flushbonading.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/4/29 18:31
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MenuDTO {
    private int id;
    private String label;
    private Boolean checked = false;
    private int parent_id;
    private List<MenuDTO> children;
}
