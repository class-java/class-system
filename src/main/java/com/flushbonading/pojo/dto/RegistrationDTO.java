package com.flushbonading.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/4/13 15:14
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationDTO {
    private String name;
    private String student_number;
    private Integer gender;
    private String class_name;
    private String c_language;
    private String circuit;
    private String experience;
    private String skill;
    private Integer admit;
}
