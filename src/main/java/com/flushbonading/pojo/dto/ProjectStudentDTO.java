package com.flushbonading.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/7 9:35
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjectStudentDTO {
    //组名
    private String group_name;
    //学号
    private String student_number;
    //姓名
    private String student_name;
    //工作方向
    private String technical_direction;
    //项目职位
    private String job_position;
}
