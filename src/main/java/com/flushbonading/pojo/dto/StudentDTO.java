package com.flushbonading.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/4/28 21:07
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentDTO {

    private String student_name;    //学生姓名
    private String sex;             //性别
    private String department;      //系部
    private String class_name;           //原班级
    private String original_student_number; //原学号
    private String nationality;     //民族
    private String identification_card;//身份证
    private String political_status;   //政治面貌
    private String contact_number;     //联系电话
    private String qq_number;           //qq号
    private String email;              //e-mail
    private String bank_account;       //银行账号
    private String card_number;        //一卡通号
    private String family_address;     //家庭住址
    private String postcode;           //邮编
    private String parent_name;        //家长姓名
    private String parent_phone;        //家长电话
}
