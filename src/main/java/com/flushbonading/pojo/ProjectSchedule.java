package com.flushbonading.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/12 9:37
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_project_schedule")
@ApiModel(value = "ProjectSchedule对象", description = "")
@Builder
public class ProjectSchedule implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "name")
    private String name;
    @ApiModelProperty(value = "schedule")
    private String schedule;
    @ApiModelProperty(value = "project_id")
    private Integer project_id;
    @ApiModelProperty(value = "up_time")
    private String up_time;
}
