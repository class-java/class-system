package com.flushbonading.validator;

import com.alibaba.druid.util.StringUtils;
import com.flushbonading.annotation.StudentNumber;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author zyx 2134208960@qq.com
 * @version 0.1.0
 * @create 2021-03-13 09:40
 * @since 0.1.0
 **/
public class StudentNumberValidator implements ConstraintValidator<StudentNumber, String> {
    @Override
    public void initialize(final StudentNumber constraintAnnotation) {

    }

    @Override
    public boolean isValid(final String string, final ConstraintValidatorContext constraintValidatorContext) {
        if (string == null) {
            return false;
        }
        if (string.length() <= 11) {
            return StringUtils.isNumber(string);
        }
        return false;
    }
}
