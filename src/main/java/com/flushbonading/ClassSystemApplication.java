package com.flushbonading;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;


@SpringBootApplication
@EnableCaching
@MapperScan("com.flushbonading.mapper")
public class ClassSystemApplication {

    public static void main(final String[] args) {
        SpringApplication.run(ClassSystemApplication.class, args);
    }
}
