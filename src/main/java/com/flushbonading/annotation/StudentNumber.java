package com.flushbonading.annotation;

import com.flushbonading.validator.StudentNumberValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author zyx 2134208960@qq.com
 * @version 0.1.0
 * @create 2021-03-12 17:59
 * @since 0.1.0
 **/
@Documented
@Retention(RetentionPolicy.RUNTIME)	//在运行时生效
@Target({ElementType.FIELD,ElementType.PARAMETER})	//作用在字段或参数上
@Constraint(validatedBy = StudentNumberValidator.class)
public @interface StudentNumber {
    String[] value() default "";    //注解里面的属性
    String message() default "必须填写正确的学号";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
