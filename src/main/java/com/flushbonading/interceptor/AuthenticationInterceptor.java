package com.flushbonading.interceptor;

import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flushbonading.util.TokenUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zyx 2134208960@qq.com
 * @version 0.1.0
 * @create 2021-03-16 11:21
 * @since 0.1.0
 **/

public class AuthenticationInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler) throws Exception {
        System.out.println(request.getRequestURI());
        if (request.getRequestURI().contains("face")) {
            return true;
        }
        final String token = request.getHeader("token");
        if (token == null) {
            return false;
        }
        System.out.println(token);
        System.out.println(request.getRequestURI());
        final Map<String, Object> map = new HashMap<>();
        try {
            //检查token合法性
            TokenUtils.verify(token);
            return true;
        } catch (final SignatureVerificationException e) {
            e.printStackTrace();
            map.put("msg", "签名无效");
        } catch (final TokenExpiredException e) {
            e.printStackTrace();
            map.put("msg", "token过期");
        } catch (final Exception e) {
            e.printStackTrace();
            map.put("msg", "token无效");
        }
        map.put("state", false);
        final String json = new ObjectMapper().writeValueAsString(map);
        response.setContentType("application/json;charset=UTF-8");
        return false;

    }

    @Override
    public void postHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler, final ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(final HttpServletRequest request, final HttpServletResponse response, final Object handler, final Exception ex) throws Exception {

    }
}
