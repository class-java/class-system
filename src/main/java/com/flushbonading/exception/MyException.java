package com.flushbonading.exception;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/4/28 10:17
 * @since 1.0.0
 */
public class MyException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * 提供无参数的构造方法
     */

    public MyException() {
    }

    /**
     * 提供一个有参数的构造方法，可自动生成
     */
    public MyException(final String message) {
        // 把参数传递给Throwable的带String参数的构造方法
        super(message);
    }

}
