//package com.flushbonading.config;
//
//
//import cn.hutool.extra.spring.SpringUtil;
//import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.ibatis.cache.Cache;
//import org.springframework.data.redis.connection.RedisServerCommands;
//import org.springframework.data.redis.core.RedisCallback;
//import org.springframework.data.redis.core.RedisTemplate;
//
//import java.util.Set;
//import java.util.concurrent.locks.ReadWriteLock;
//import java.util.concurrent.locks.ReentrantReadWriteLock;
//
///**
// * @author zyx 2134208960@qq.com
// * @date 2021/4/24 16:31
// * @since 1.0.0
// */
//@Slf4j
//public class MybatisRedisCache implements Cache {
//
//
//    /**
//     * 读写锁
//     */
//    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock(true);
//
//    //这里使用了redis缓存，使用springboot自动注入
//    private RedisTemplate<String, Object> redisTemplate;
//
//    private String id;
//
//    public MybatisRedisCache(final String id) {
//        if (id == null) {
//            throw new IllegalArgumentException("Cache instances require an ID");
//        }
//        this.id = id;
//    }
//
//    @Override
//    public String getId() {
//        return this.id;
//    }
//
//    @Override
//    public void putObject(final Object key, final Object value) {
//        if (this.redisTemplate == null) {
//            //由于启动期间注入失败，只能运行期间注入，这段代码可以删除
//            this.redisTemplate = (RedisTemplate<String, Object>) SpringUtil.getBean("redisTemplate");
//        }
//        if (value != null) {
//            this.redisTemplate.opsForValue().set(key.toString(), value);
//        }
//    }
//
//    @Override
//    public Object getObject(final Object key) {
//        if (this.redisTemplate == null) {
//            //由于启动期间注入失败，只能运行期间注入，这段代码可以删除
//            this.redisTemplate = (RedisTemplate<String, Object>) SpringUtil.getBean("redisTemplate");
//        }
//        try {
//            if (key != null) {
//                return this.redisTemplate.opsForValue().get(key.toString());
//            }
//        } catch (final Exception e) {
//            e.printStackTrace();
//            log.error("缓存出错 ");
//        }
//        return null;
//    }
//
//    @Override
//    public Object removeObject(final Object key) {
//        if (this.redisTemplate == null) {
//            //由于启动期间注入失败，只能运行期间注入，这段代码可以删除
//            this.redisTemplate = (RedisTemplate<String, Object>) SpringUtil.getBean("redisTemplate");
//        }
//        if (key != null) {
//            this.redisTemplate.delete(key.toString());
//        }
//        return null;
//    }
//
//    @Override
//    public void clear() {
//        log.debug("清空缓存");
//        if (this.redisTemplate == null) {
//            this.redisTemplate = (RedisTemplate<String, Object>) SpringUtil.getBean("redisTemplate");
//        }
//        final Set<String> keys = this.redisTemplate.keys("*:" + this.id + "*");
//        if (!CollectionUtils.isEmpty(keys)) {
//            this.redisTemplate.delete(keys);
//        }
//    }
//
//    @Override
//    public int getSize() {
//        if (this.redisTemplate == null) {
//            //由于启动期间注入失败，只能运行期间注入，这段代码可以删除
//            this.redisTemplate = (RedisTemplate<String, Object>) SpringUtil.getBean("redisTemplate");
//        }
//        final Long size = this.redisTemplate.execute((RedisCallback<Long>) RedisServerCommands::dbSize);
//        return size.intValue();
//    }
//
//    @Override
//    public ReadWriteLock getReadWriteLock() {
//        return this.readWriteLock;
//    }
//}
