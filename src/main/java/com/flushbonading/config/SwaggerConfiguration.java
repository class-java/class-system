package com.flushbonading.config;

import com.flushbonading.annotation.BackStage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * Swagger的配置类
 * @Author zlx&zyx
 * @description
 * @Version 1.0
 * @Date 2021/3/31 20:39
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket createRestApi(){
        //后台管理的接口文档
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("后台管理接口")
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(BackStage.class))
                .paths(PathSelectors.any())
                .build()
                //Swagger提供了全局登录功能
                .securityContexts(securityContexts())
                .securitySchemes(securitySchemes());
    }
    @Bean
    public Docket createRestApi2(){
        //门户的接口文档
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo2())
                .groupName("门户接口")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.flushbonading.controller.face"))
                .paths(PathSelectors.any())
                .build()
                //Swagger提供了全局登录功能
                .securityContexts(securityContexts())
                .securitySchemes(securitySchemes());
    }
    private ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                .title("嵌入式班级管理系统")
                .description("后台管理接口").build();
    }

    private ApiInfo apiInfo2(){
        return new ApiInfoBuilder()
                .title("嵌入式班级管理系统")
                .description("门户接口").build();
    }
    //写securityContexts
    private List<SecurityContext> securityContexts(){
        //设置需要登录认证的路径
        List<SecurityContext> result=new ArrayList<>();
        result.add(getContextByPath("/Login/.*"));
        return result;
    }

    //使用正则匹配
    private SecurityContext getContextByPath(String pathRegex) {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.regex(pathRegex))
                .build();
    }

    //??????
    private List<SecurityReference> defaultAuth() {
        List<SecurityReference> result = new ArrayList<>();
        AuthorizationScope authorizationScope= new AuthorizationScope("global","accessEverything");
        AuthorizationScope[] authorizationScopes=new AuthorizationScope[1];
        authorizationScopes[0]=authorizationScope;
        result.add(new SecurityReference("Authorization",authorizationScopes));
        return result;
    }

    //写securitySchemes
    private List<ApiKey> securitySchemes(){
        //设置请求头信息
        List<ApiKey> result=new ArrayList<>();
        ApiKey apiKey=new ApiKey("token","token","Header");
        result.add(apiKey);
        return result;

    }

}

