//package com.flushbonading.config;
//
//import com.flushbonading.util.BuildTableJob;
//import org.quartz.*;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * @author zk
// * @date 2021/10/23 11:24
// */
//@Configuration
//public class QuartzConfig {
//    /**
//     * 创建Job实例
//     *
//     * @return
//     */
//    @Bean
//    public JobDetail createJobDetail() {
//        //业务类
//        return JobBuilder.newJob(BuildTableJob.class)
//                //可以给该JobDetail起一个id
//                .withIdentity("QuartJob")
//                //每个JobDetail内都有一个Map，包含了关联到这个Job的数据，在Job类中可以通过context获取
//                .usingJobData("msg", "Hello Quartz!")
//                .storeDurably()//即使没有Trigger关联时，也不需要删除该JobDetail
//                .build();
//    }
//
//    /**
//     * 创建触发器
//     *
//     * @return
//     */
//    @Bean
//    public Trigger createTrigger() {
//        final CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule("0 0 0 1 9 ? 2019-2030");
//        return TriggerBuilder.newTrigger()
//                //关联上述的JobDetail
//                .forJob(createJobDetail())
//                //给Trigger起个名字
//                .withIdentity("quartzTaskService")
//                .withSchedule(cronScheduleBuilder)
//                .build();
//    }
//
//}
