package com.flushbonading.util;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/4/25 13:40
 * @since 1.0.0
 */
public enum DirectionUtil {
    PUBLIC("公共课",123),
    EMBEDDED_SYSTEM("嵌入式系统",12),
    APPLICATION("应用端软件",3);
    private String name;
    private int value;
    private DirectionUtil(String name, int value) {
        this.name = name;
        this.value = value;
    }
    public static int directionID(String name){
        for (DirectionUtil m : DirectionUtil.values()) {
            if (m.getName().equals(name)) {
                return m.getValue();
            }
        }
        return -1;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
