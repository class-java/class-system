package com.flushbonading.util;

import java.util.Calendar;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/14 16:50
 * @since 1.0.0
 */
public class GradeUtil {
    public static int getGrade(){
        Calendar instance = Calendar.getInstance();
        return instance.get(Calendar.YEAR)-3;
    }
}
