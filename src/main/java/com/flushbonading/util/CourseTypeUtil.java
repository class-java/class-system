package com.flushbonading.util;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/10 11:13
 * @since 1.0.0
 */
public enum CourseTypeUtil {

    DEGREE_COURSE("学位课/必修课",1),
    PROFESSIONAL_COURSE("专业课/任选课",2),
    COMMON_REQUIRED_COURSE("公共课/任选课",3),
    CURRICULUM_DESIGN("课程设计",4),
    SKILL_TRAIN("技能训练",5),
    INNOVATION_REQUIRED_COURSE("创新创业课/必修课",6),
    INNOVATION_OPTIONAL_COURSE("创新创业课/任选课",7);

    private String name;
    private int value;

    private CourseTypeUtil(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public static int courseTypeID(String courseType){
        for (CourseTypeUtil m : CourseTypeUtil.values()) {
            if (m.getName().equals(courseType)) {
                return m.getValue();
            }
        }
        return -1;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
