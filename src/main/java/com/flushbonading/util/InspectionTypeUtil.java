package com.flushbonading.util;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/10 14:26
 * @since 1.0.0
 */
public enum InspectionTypeUtil {
    INSPECT("考查",1),
    EXAMINATION("考试",2);

    private String name;
    private int value;

    private InspectionTypeUtil(String name, int value) {
        this.name = name;
        this.value = value;
    }
    public static int InspectionTypeID(String inspectionType){
        for (InspectionTypeUtil m : InspectionTypeUtil.values()) {
            if (m.getName().equals(inspectionType)) {
                return m.getValue();
            }
        }
        return -1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
