package com.flushbonading.util;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/10 18:33
 * @since 1.0.0
 */
public enum CourseCharacterUtil {

    INITIAL_REPAIR("初修",1),
    REBUILD("重修",2);

    private String name;
    private int value;

    private CourseCharacterUtil(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public static int courseCharacterID(String CourseCharacter){
        for (CourseCharacterUtil m : CourseCharacterUtil.values()) {
            if (m.getName().equals(CourseCharacter)) {
                return m.getValue();
            }
        }
        return -1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
