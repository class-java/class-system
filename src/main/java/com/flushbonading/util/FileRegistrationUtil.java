package com.flushbonading.util;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.metadata.style.WriteFont;
import com.alibaba.excel.write.style.HorizontalCellStyleStrategy;
import com.flushbonading.pojo.dto.FileRegistrationDTO;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author zk
 * @date 2021/9/28 14:33
 */
public class FileRegistrationUtil {
    public static void writeExcel(final HttpServletResponse response, final List<FileRegistrationDTO> list) throws IOException {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
        final String datetime = sdf.format(new Date());
        final String fileName = URLEncoder.encode("嵌入式班报名信息", "UTF-8") + datetime;

        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        //这里可以设置浏览器内部下载还是附件下载2种方式
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");

        // excel头策略(可省略)
        final WriteCellStyle headWriteCellStyle = new WriteCellStyle();
        final WriteFont headWriteFont = new WriteFont();
        headWriteFont.setFontHeightInPoints((short) 11);
        headWriteFont.setBold(false);
        headWriteCellStyle.setWriteFont(headWriteFont);

        // excel内容策略(可省略)
        final WriteCellStyle contentWriteCellStyle = new WriteCellStyle();
        final WriteFont contentWriteFont = new WriteFont();
        contentWriteFont.setFontHeightInPoints((short) 11);
        contentWriteCellStyle.setWriteFont(contentWriteFont);

        // 设置handler
        final HorizontalCellStyleStrategy styleStrategy = new HorizontalCellStyleStrategy(headWriteCellStyle, contentWriteCellStyle);

        EasyExcel.write(response.getOutputStream(), FileRegistrationDTO.class)
                .sheet("报名信息")
                .registerWriteHandler(styleStrategy)
                .doWrite(list);
    }
}
