package com.flushbonading.util;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/9 16:07
 * @since 1.0.0
 */
public enum OperatingPostUtil {

    PROJECT_MANAGER("项目经理",1),
    PROJECT_SECRETARY("项目秘书",2),
    DEVELOPER("开发人员",3),
    TESTER("测试人员",4);

    private String name;
    private int value;
    private OperatingPostUtil(String name,int value){
        this.name = name;
        this.value = value;
    }
    public static int operatingPostID(String name){
        for (OperatingPostUtil m : OperatingPostUtil.values()) {
            if (m.getName().equals(name)) {
                return m.getValue();
            }
        }
        return -1;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
