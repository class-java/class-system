package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.Status;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/18 20:53
 * @since 1.0.0
 */
public interface IStatusService extends IService<Status> {
}
