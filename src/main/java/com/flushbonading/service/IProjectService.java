package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.Project;
import com.flushbonading.pojo.dto.ProjectDTO;
import com.flushbonading.pojo.dto.ProjectListDAO;
import com.flushbonading.pojo.dto.ProjectStudentDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
public interface IProjectService extends IService<Project> {
    /**
     * 项目id查询项目名
     *
     * @param project_id
     * @return
     */
    String selectProjectByProject(Integer project_id);

    int getLastProjectID();

    List<ProjectDTO> selectAll();

    int insertAProject(ProjectDTO projectDTO);

    int deleteProject(int project_id);

    int updateGroupName(int project_id, String group_name);

    List<ProjectStudentDTO> selectStudentByID(int project_id);

    int deleteAStudent(String student_number);

    int updateProject(int project_id, ProjectDTO projectDTO);

    String selectIntroduction(String student_number);

    int updateStudent(@Param("projectStudentDTO") ProjectStudentDTO projectStudentDTO, int project_id, int operating_post_id);

    List<ProjectListDAO> selectAllProjects();
}
