package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.Grade;
import com.flushbonading.pojo.dto.GradeDTO;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
public interface IGradeService extends IService<Grade> {
    List<GradeDTO> getList();
}
