package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.OperatingPost;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/6 17:08
 * @since 1.0.0
 */
public interface IOperatingPostService extends IService<OperatingPost> {
    public int getIDByName(String operating_post_name);
}
