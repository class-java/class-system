package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.Course;
import com.flushbonading.pojo.dto.CourseDTO;
import com.flushbonading.pojo.dto.CourseMessageDTO;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
public interface ICourseService extends IService<Course> {
    List<CourseMessageDTO> getAllMessage();

    List<CourseMessageDTO> getCourseByStudyDirection(int study_direction_id);

    int insertACourse(int index, String course_name, String teacherName, String type, Double scorePercentage);

    int deleteByCourseID(int course_id);

    int getLastCourseID();

    Integer counts(String course_name);

    CourseDTO getDetailsByID(int course_id);

    int updateDetailsByID(int course_id, CourseDTO courseDTO);
}
