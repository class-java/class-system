package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.Registration;
import com.flushbonading.pojo.RespBean;
import com.flushbonading.pojo.dto.FileRegistrationDTO;
import com.flushbonading.pojo.dto.RegistrationTempDTO;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zk
 * @since 2021-09-24
 */
public interface RegistrationService extends IService<Registration> {
    /**
     * 添加报名人员信息
     *
     * @param registrationTempDTO 报名人员信息
     * @return 返回给前端信息
     */
    RespBean insertRegistration(RegistrationTempDTO registrationTempDTO);

    /**
     * 获得t_registration表中的所有学生信息
     *
     * @return 学生信息集合
     */
    List<RegistrationTempDTO> getRegistrationTempDto();

    /**
     * 获得t_registration表中对应的FileRegistrationDTO所有学生信息
     * 用于导出excel文件
     *
     * @return 学生信息集合
     */
    List<FileRegistrationDTO> getFileRegistrationDto();

    /**
     * 录取报名学生，admit==2时录取
     *
     * @param registrationTempDTO 学生信息
     * @return 返回一个判断是否修改成功的值，成功大于0
     */
    RespBean updateRegistrationAdmit(List<RegistrationTempDTO> registrationTempDTO);

    /**
     * 通过学号修改信息
     *
     * @param registrationTempDTO 学生信息
     * @return 返回一个判断是否修改成功的值，成功大于0
     */
    RespBean updateRegistrationData(RegistrationTempDTO registrationTempDTO);
}
