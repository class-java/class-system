package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.Association;
import com.flushbonading.pojo.dto.AssociationDTO;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/18 20:45
 * @since 1.0.0
 */
public interface IAssociationService extends IService<Association> {
    /**
     * 插入一个学生
     *
     * @param associationDTO 从前端控制器传过来的对象
     * @return 返回插入的数量
     */
    Integer insertAStudent(AssociationDTO associationDTO);

    /**
     * 通过学生学号拿到该学号出现的次数
     *
     * @param student_number 学生学号
     * @return int类型
     */
    int getCountsByStudentNumber(String student_number);
}
