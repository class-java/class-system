package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.Role;
import com.flushbonading.pojo.dto.RoleDTO;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zlx&zyx
 * @since 2021-04-07
 */
public interface IRoleService extends IService<Role> {
    /**
     * 通过role_id返回role名
     *
     * @param role_id
     * @return 返回role名
     **/
    String getNameByRoleId(int role_id);

    List<RoleDTO> getAllRoleMessage();

    int updateRole(RoleDTO roleDTO);

    int insertRole(RoleDTO roleDTO);

    int deleteRoleByID(int role_id);

    List<RoleDTO> searchRole(String role_id, String role_name);

    List<RoleDTO> getAllRoleName();
}
