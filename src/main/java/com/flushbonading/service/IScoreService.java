package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.Score;
import com.flushbonading.pojo.dto.ScoreDTO;
import com.flushbonading.pojo.dto.ScoreMessageDTO;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
public interface IScoreService extends IService<Score> {
    List<ScoreDTO> getSocresByCourseID(int course_id);

    int updateCourses(int course_id, ScoreDTO scoreDTO);

    int updateAllScore();

    int updateTotalScore(int course_id, ScoreDTO scoreDTO);

    int deleteScoreByID(int course_id);

    int deleteAScore(int course_id, String student_number);

    int insertCourse(int course_id, String type);

    List<ScoreDTO> searchScore(int course_id, String student_number, String name);

    List<ScoreMessageDTO> getScoresByStudentNumber(String student_number);
}
