package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.Menu;
import com.flushbonading.pojo.dto.MenuDTO;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
public interface IMenuService extends IService<Menu> {
    /**
     * 同国用户id获得菜单
     *
     * @param user_id
     * @return
     */
    List<Menu> getMenu(String user_id);

    /**
     * 通过parentId获得菜单子列表
     *
     * @param id
     * @return
     */
    List<Menu> getAllByParentId(Integer id);

    /**
     * 通过role_id获得菜单列表
     *
     * @param role_id
     * @return
     */
    List<Menu> getRoleMenu(Integer role_id);

    List<MenuDTO> getAllPermission();

    List<MenuDTO> selectMenuByRoleID(int role_id);
}
