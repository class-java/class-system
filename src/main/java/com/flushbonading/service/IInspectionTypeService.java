package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.InspectionType;

import java.util.List;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/10 10:58
 * @since 1.0.0
 */
public interface IInspectionTypeService extends IService<InspectionType> {
    List<InspectionType> getAll();
}
