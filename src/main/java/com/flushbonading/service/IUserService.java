package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.User;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zlx
 * @since 2021-04-08
 */
public interface IUserService extends IService<User> {
    /**
     * 通过id获取用户信息
     *
     * @param userId
     * @return
     */
    User getUserId(String userId);

    /**
     * 更新用户信息
     *
     * @param user
     */
    void updateUser(User user);

    /**
     * 同user_id查询用户权限
     *
     * @param user_id
     * @return
     */
    User getRoleByUserId(String user_id);

    int selectCountByRoleID(int role_id);

    int deleteAUser(String student_number);
}
