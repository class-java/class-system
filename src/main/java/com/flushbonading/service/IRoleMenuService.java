package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.RoleMenu;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
public interface IRoleMenuService extends IService<RoleMenu> {
    int insertRoleMenu(int role_id, List<Integer> menus);
}
