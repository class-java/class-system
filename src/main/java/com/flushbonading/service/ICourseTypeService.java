package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.CourseType;

import java.util.List;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/10 10:53
 * @since 1.0.0
 */
public interface ICourseTypeService extends IService<CourseType> {
    List<CourseType> getAll();
}
