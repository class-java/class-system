package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.ProjectSchedule;
import com.flushbonading.pojo.dto.ScheduleDTO;

import java.util.List;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/12 9:47
 * @since 1.0.0
 */
public interface IProjectScheduleService extends IService<ProjectSchedule> {
    int insertSchedule(ScheduleDTO scheduleDTO);

    List<ScheduleDTO> getAllSchedule(int project_id);

    List<ScheduleDTO> getAllScheduleByStudentNumber(String student_number);

    int deleteASchedule(ScheduleDTO scheduleDTO);
}
