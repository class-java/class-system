package com.flushbonading.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.CourseMapper;
import com.flushbonading.mapper.ScoreMapper;
import com.flushbonading.mapper.TeacherMapper;
import com.flushbonading.pojo.Course;
import com.flushbonading.pojo.dto.CourseDTO;
import com.flushbonading.pojo.dto.CourseMessageDTO;
import com.flushbonading.service.ICourseService;
import com.flushbonading.util.CourseTypeUtil;
import com.flushbonading.util.DirectionUtil;
import com.flushbonading.util.InspectionTypeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
@CacheConfig(cacheNames = "course")
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements ICourseService {
    @Autowired
    private CourseMapper courseMapper;
    @Autowired
    private ScoreMapper scoreMapper;
    @Autowired
    private TeacherMapper teacherMapper;

    @Cacheable(key = "'getAllMessage'")
    @Override
    public List<CourseMessageDTO> getAllMessage() {
        return this.courseMapper.getAllMessage();
    }

    @Cacheable(key = "'study'+#study_direction_id")
    @Override
    public List<CourseMessageDTO> getCourseByStudyDirection(final int study_direction_id) {
        return this.courseMapper.getCourseByStudyDirection(study_direction_id);
    }

    @Override
    public int insertACourse(final int index, final String course_name, final String teacherName, final String type, final Double scorePercentage) {
        final CourseMessageDTO build = CourseMessageDTO.builder()
                .course_id(index)
                .course_name(course_name)
                .teacherName(teacherName)
                .study_direction_id(DirectionUtil.directionID(type))
                .scorePercentage(scorePercentage)
                .build();
        return this.courseMapper.insertACourse(build);
    }

    @CacheEvict(value = {"course", "score"}, allEntries = true)
    @Override
    public int deleteByCourseID(final int course_id) {
        return this.courseMapper.deleteByCourseID(course_id);
    }


    @Override
    public int getLastCourseID() {
        return this.courseMapper.getLastCourseID();
    }


    @Override
    public Integer counts(final String course_name) {
        return this.courseMapper.counts(course_name);
    }

    @Cacheable(key = "'course'+#course_id")
    @Override
    public CourseDTO getDetailsByID(final int course_id) {
        final CourseDTO detailsByID = this.courseMapper.getDetailsByID(course_id);
        final Double exam_score = detailsByID.getExam_score();
        final DecimalFormat df = new DecimalFormat("0.0");
        detailsByID.setUsually_score(Double.valueOf(df.format(1 - exam_score)));
        return detailsByID;
    }

    @CacheEvict(value = {"course", "score"}, allEntries = true)
    @Override
    public int updateDetailsByID(final int course_id, final CourseDTO courseDTO) {
        final int teacherID = this.teacherMapper.getIdByTeacherName(courseDTO.getTeacher_name());
        final int i = this.courseMapper.updateDetailsByID(course_id, courseDTO.getCourse_name(), teacherID, courseDTO.getCredit(), CourseTypeUtil.courseTypeID(courseDTO.getCategory()), InspectionTypeUtil.InspectionTypeID(courseDTO.getAssessment_method()), courseDTO.getExam_score());
        return i;
    }

}
