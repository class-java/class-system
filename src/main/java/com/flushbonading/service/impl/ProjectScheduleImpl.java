package com.flushbonading.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.ProjectScheduleMapper;
import com.flushbonading.pojo.ProjectSchedule;
import com.flushbonading.pojo.dto.ScheduleDTO;
import com.flushbonading.service.IProjectScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/12 9:50
 * @since 1.0.0
 */
@CacheConfig(cacheNames = "project")
@Service
public class ProjectScheduleImpl extends ServiceImpl<ProjectScheduleMapper, ProjectSchedule> implements IProjectScheduleService {

    @Autowired
    private ProjectScheduleMapper projectScheduleMapper;

    @Override
    public int insertSchedule(final ScheduleDTO scheduleDTO) {
        return this.projectScheduleMapper.insertSchedule(scheduleDTO);
    }

    @Cacheable(key = "'getAllSchedule' + #project_id")
    @Override
    public List<ScheduleDTO> getAllSchedule(final int project_id) {
        return this.projectScheduleMapper.getAllSchedule(project_id);
    }

    @Cacheable(key = "'getAllScheduleByStudentNumber'+#student_number")
    @Override
    public List<ScheduleDTO> getAllScheduleByStudentNumber(final String student_number) {
        return this.projectScheduleMapper.getAllScheduleByStudentNumber(student_number);
    }

    @CacheEvict(allEntries = true)
    @Override
    public int deleteASchedule(final ScheduleDTO scheduleDTO) {
        return this.projectScheduleMapper.deleteASchedule(scheduleDTO);
    }
}
