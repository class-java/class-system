package com.flushbonading.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.AssociationMapper;
import com.flushbonading.pojo.Association;
import com.flushbonading.pojo.dto.AssociationDTO;
import com.flushbonading.service.IAssociationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/18 20:48
 * @since 1.0.0
 */
@Service
public class AssociationImpl extends ServiceImpl<AssociationMapper, Association> implements IAssociationService {
    @Autowired
    private AssociationMapper associationMapper;

    @Override
    public Integer insertAStudent(AssociationDTO associationDTO) {
        return associationMapper.insertAStudent(associationDTO);
    }

    @Override
    public int getCountsByStudentNumber(String student_number) {
        return associationMapper.getCountsByStudentNumber(student_number);
    }
}
