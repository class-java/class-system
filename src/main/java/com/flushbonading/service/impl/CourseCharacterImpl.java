package com.flushbonading.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.CourseCharacterMapper;
import com.flushbonading.pojo.CourseCharacter;
import com.flushbonading.service.ICourseCharacterService;
import org.springframework.stereotype.Service;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/10 11:07
 * @since 1.0.0
 */
@Service
public class CourseCharacterImpl extends ServiceImpl<CourseCharacterMapper, CourseCharacter> implements ICourseCharacterService {
}
