package com.flushbonading.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.StudyDirectionMapper;
import com.flushbonading.pojo.StudyDirection;
import com.flushbonading.service.IStudyDirectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
@Service
public class StudyDirectionServiceImpl extends ServiceImpl<StudyDirectionMapper, StudyDirection> implements IStudyDirectionService {
    @Autowired
    private StudyDirectionMapper studyDirectionMapper;

    /**
     * 学习方向id查询查询学习方向
     *
     * @param study_direction_id
     * @return
     */
    @Override
    public String selectStudyDirection(final Integer study_direction_id) {

        return this.studyDirectionMapper.selectStudyDirection(study_direction_id);
    }
}
