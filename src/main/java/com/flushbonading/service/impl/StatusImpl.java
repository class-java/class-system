package com.flushbonading.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.StatusMapper;
import com.flushbonading.pojo.Status;
import com.flushbonading.service.IStatusService;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/18 20:54
 * @since 1.0.0
 */
public class StatusImpl extends ServiceImpl<StatusMapper, Status> implements IStatusService {
}
