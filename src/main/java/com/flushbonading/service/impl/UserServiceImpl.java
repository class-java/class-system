package com.flushbonading.service.impl;

import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.UserMapper;
import com.flushbonading.pojo.User;
import com.flushbonading.pojo.dto.UserDTO;
import com.flushbonading.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zlx
 * @since 2021-04-08
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Resource
    private UserMapper userMapper;

    /**
     * 登录时进行验证
     *
     * @param userDTO
     * @return
     */
    public User Login(final UserDTO userDTO) {
        final QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userDTO.getId());
        //md5加密验证
        wrapper.eq("password", SecureUtil.md5(userDTO.getPassword()));

        final User user = this.userMapper.selectOne(wrapper);
        System.out.println(user);
        return user;
    }

    @Override
    public User getUserId(final String userId) {
        return this.userMapper.getUserId(userId);
    }

    /**
     * 更新用户信息
     *
     * @param user
     */

    @Override
    public void updateUser(final User user) {
        user.setPassword(SecureUtil.md5(user.getPassword()));
        this.userMapper.updateUser(user);
    }

    /**
     * 同user_id查询用户权限
     *
     * @param user_id
     * @return
     */
    @Override
    public User getRoleByUserId(final String user_id) {

        return this.userMapper.getRoleByUserId(user_id);
    }

    @Override
    public int selectCountByRoleID(final int role_id) {
        return this.userMapper.selectCountByRoleID(role_id);
    }


    @Override
    public int deleteAUser(final String student_number) {
        return this.userMapper.deleteAUser(student_number);
    }

}
