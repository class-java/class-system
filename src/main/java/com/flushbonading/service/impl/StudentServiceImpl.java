package com.flushbonading.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.OperatingPostMapper;
import com.flushbonading.mapper.StudentMapper;
import com.flushbonading.mapper.UserMapper;
import com.flushbonading.pojo.Student;
import com.flushbonading.pojo.dto.ProjectStudentDTO;
import com.flushbonading.pojo.dto.StudentDTO;
import com.flushbonading.pojo.dto.StudentMessageDTO;
import com.flushbonading.service.IStudentService;
import com.flushbonading.util.GradeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zlx&zyx
 * @since 2021-04-07
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {

    @Autowired
    private StudentMapper studentMapper;
    @Autowired
    private OperatingPostMapper operatingPostMapper;
    @Autowired
    private UserMapper userMapper;

    /**
     * 通过学号获得学生信息
     *
     * @param user_id
     * @return
     */
    @Override
    public Student getStudent(final String user_id) {
        return this.studentMapper.getStudent(user_id);
    }

    @Override
    public String getNameByStudnetNum(final String student_number) {
        return this.studentMapper.getNameByStudentNum(student_number);
    }

    @Override
    public StudentDTO selectStudentMessage(final String student_number) {
        return this.studentMapper.selectStudentMessage(student_number);
    }

    @Override
    public int updateStudentMessage(final StudentDTO studentDTO, final String student_number) {
        return this.studentMapper.updateStudentMessage(studentDTO, student_number);
    }

    @Override
    public int updateProjectID(final int project_id) {
        return this.studentMapper.updateProjectID(project_id);
    }

    @Override
    public int updateProject(final int project_id, final ProjectStudentDTO projectStudentDTO) {
        final int operatingPostID = this.operatingPostMapper.getIDByName(projectStudentDTO.getJob_position());
        int j = -1;
        if (operatingPostID > 0) {
            j = this.studentMapper.updateProject(projectStudentDTO.getStudent_number(), project_id, operatingPostID, projectStudentDTO.getTechnical_direction());
        }
        return j;
    }

    @Override
    public int atTheProjectCounts(final String student_number) {
        return this.studentMapper.atTheProjectCounts(student_number);
    }

    @Override
    public Integer getProjectIDByStudentNumber(final String student_number) {
        return this.studentMapper.getProjectIDByStudentNumber(student_number);
    }

    @Override
    public String getName(final String student_number) {
        return this.studentMapper.getName(student_number);
    }

    @Override
    public List<StudentMessageDTO> getAllStudent() {
        return this.studentMapper.getAllStudent();
    }

    @Override
    public int deleteAStudent(final String student_number) {
        return this.studentMapper.deleteAStudent(student_number);
    }

    @Override
    public int updateStudentMessageByStudentNumber(final StudentMessageDTO studentMessageDTO) {
        return this.studentMapper.updateStudentMessageByStudentNumber(studentMessageDTO);
    }

    @Override
    public int insertAStudent(final StudentMessageDTO studentMessageDTO) {
        final int i = this.studentMapper.insertAStudent(GradeUtil.getGrade(), studentMessageDTO);
        int j = -1;
        if (i > 0) {
            j = this.userMapper.insertAUser(studentMessageDTO.getStudent_number(), studentMessageDTO.getRole_name());
        }
        return j;
    }

    @Override
    public List<StudentMessageDTO> searchByName(final String name) {
        return this.studentMapper.searchByName(name);
    }
}
