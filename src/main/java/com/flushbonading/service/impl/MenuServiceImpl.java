package com.flushbonading.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.MenuMapper;
import com.flushbonading.pojo.Menu;
import com.flushbonading.pojo.dto.MenuDTO;
import com.flushbonading.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {
    @Autowired
    private MenuMapper menuMapper;

    /**
     * 同国用户id获得菜单
     *
     * @param user_id
     * @return
     */
    @Override
    public List<Menu> getMenu(final String user_id) {
        return this.menuMapper.getMenu(user_id);
    }

    /**
     * 通过parentId获得菜单子列表
     *
     * @param id
     * @return
     */
    @Override
    public List<Menu> getAllByParentId(final Integer id) {
        return this.menuMapper.getAllByParentId(id);
    }

    /**
     * 通过role_id获得菜单列表
     *
     * @param role_id
     * @return
     */
    @Override
    public List<Menu> getRoleMenu(final Integer role_id) {
        return this.menuMapper.getRoleMenu(role_id);
    }

    @Override
    public List<MenuDTO> getAllPermission() {
        return this.menuMapper.getAllPermission();
    }

    @Override
    public List<MenuDTO> selectMenuByRoleID(final int role_id) {
        return this.menuMapper.selectMenuByRoleID(role_id);
    }
}
