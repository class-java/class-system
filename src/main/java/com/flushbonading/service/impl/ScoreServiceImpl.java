package com.flushbonading.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.ScoreMapper;
import com.flushbonading.pojo.Score;
import com.flushbonading.pojo.dto.ScoreDTO;
import com.flushbonading.pojo.dto.ScoreMessageDTO;
import com.flushbonading.service.IScoreService;
import com.flushbonading.util.DirectionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
@CacheConfig(cacheNames = "score")
@Service
public class ScoreServiceImpl extends ServiceImpl<ScoreMapper, Score> implements IScoreService {

    @Autowired
    private ScoreMapper scoreMapper;

    @Cacheable(key = "'getSocresByCourseID' + #course_id")
    @Override
    public List<ScoreDTO> getSocresByCourseID(final int course_id) {
        return this.scoreMapper.getSocresByCourseID(course_id);
    }

    @CacheEvict(allEntries = true)
    @Override
    public int updateCourses(final int course_id, final ScoreDTO scoreDTO) {
        return this.scoreMapper.updateCourses(course_id, scoreDTO);
    }

    @CacheEvict(allEntries = true)
    @Override
    public int updateAllScore() {
        return this.scoreMapper.updateAllScore();
    }

    @CacheEvict(allEntries = true)
    @Override
    public int updateTotalScore(final int course_id, final ScoreDTO scoreDTO) {
        return this.scoreMapper.updateTotalScore(course_id, scoreDTO);
    }

    @CacheEvict(allEntries = true)
    @Override
    public int deleteScoreByID(final int course_id) {
        return this.scoreMapper.deleteCourse(course_id);
    }

    @CacheEvict(allEntries = true)
    @Override
    public int deleteAScore(final int course_id, final String student_number) {
        return this.scoreMapper.deleteAScore(course_id, student_number);
    }

    @CacheEvict(allEntries = true)
    @Override
    public int insertCourse(final int course_id, final String type) {
        return this.scoreMapper.insertCourse(course_id, DirectionUtil.directionID(type));
    }

    @Cacheable(key = "'searchScore' + #course_id + #student_number + #name")
    @Override
    public List<ScoreDTO> searchScore(final int course_id, final String student_number, final String name) {
        return this.scoreMapper.searchScore(course_id, student_number, name);
    }

    @Cacheable(key = "'getScoresByStudentNumber'")
    @Override
    public List<ScoreMessageDTO> getScoresByStudentNumber(final String student_number) {
        return this.scoreMapper.getScoresByStudentNumber(student_number);
    }
}
