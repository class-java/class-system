package com.flushbonading.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.TeacherMapper;
import com.flushbonading.pojo.Teacher;
import com.flushbonading.pojo.dto.TeacherDTO;
import com.flushbonading.service.ITeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
@CacheConfig(cacheNames = "teacher")
@Service
public class TeacherServiceImpl extends ServiceImpl<TeacherMapper, Teacher> implements ITeacherService {
    @Autowired
    private TeacherMapper teacherMapper;

    /**
     * 通过教师号得到教师名
     *
     * @param teacher_id 教师工号
     * @return 返回教师名
     **/
    @Cacheable(key = "'teacher'+#teacher_id")
    @Override
    public String getNameByTeacherId(final String teacher_id) {
        return this.teacherMapper.getNameByTeacherId(teacher_id);
    }

    @Cacheable(key = "'getAllTeacher'")
    @Override
    public List<TeacherDTO> getAllTeacher() {
        return this.teacherMapper.getAllTeacher();
    }
}
