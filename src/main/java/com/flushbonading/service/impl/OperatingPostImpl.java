package com.flushbonading.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.OperatingPostMapper;
import com.flushbonading.pojo.OperatingPost;
import com.flushbonading.service.IOperatingPostService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/6 17:10
 * @since 1.0.0
 */
public class OperatingPostImpl extends ServiceImpl<OperatingPostMapper, OperatingPost> implements IOperatingPostService {
    @Autowired
    private OperatingPostMapper operatingPostMapper;
    @Override
    public int getIDByName(String operating_post_name) {
        return operatingPostMapper.getIDByName(operating_post_name);
    }
}
