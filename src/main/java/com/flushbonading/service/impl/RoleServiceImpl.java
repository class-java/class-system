package com.flushbonading.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.RoleMapper;
import com.flushbonading.mapper.RoleMenuMapper;
import com.flushbonading.pojo.Role;
import com.flushbonading.pojo.dto.RoleDTO;
import com.flushbonading.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private RoleMenuMapper roleMenuMapper;

    /**
     * 通过role_id返回role名
     *
     * @param role_id
     * @return 返回role名
     **/
    @Override
    public String getNameByRoleId(final int role_id) {
        return this.roleMapper.getNameByRoleId(role_id);
    }

    @Override
    public List<RoleDTO> getAllRoleMessage() {
        return this.roleMapper.getAllRoleMessage();
    }

    @Override
    public int updateRole(final RoleDTO roleDTO) {
        return this.roleMapper.updateRole(roleDTO);
    }

    @Override
    public int insertRole(final RoleDTO roleDTO) {
        final int lastID = this.roleMapper.getLastID();
        int j = 1;
        if (lastID > 0) {
            roleDTO.setRole_id(lastID + 1);
            j = this.roleMapper.insertRole(roleDTO);
        }
        return j;
    }

    @Override
    public int deleteRoleByID(final int role_id) {
        final int i = this.roleMenuMapper.deleteRoleMenuByID(role_id);
        int j = -1;
        if (i >= 0) {
            j = this.roleMapper.deleteRoleByID(role_id);
        }
        return j;
    }

    @Override
    public List<RoleDTO> searchRole(final String role_id, final String role_name) {
        return this.roleMapper.searchRole(role_id, role_name);
    }

    @Override
    public List<RoleDTO> getAllRoleName() {
        return this.roleMapper.getAllRoleName();
    }
}
