package com.flushbonading.service.impl;

import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.RegistrationMapper;
import com.flushbonading.mapper.SystemConfigMapper;
import com.flushbonading.mapper.UserMapper;
import com.flushbonading.pojo.Registration;
import com.flushbonading.pojo.RespBean;
import com.flushbonading.pojo.dto.*;
import com.flushbonading.service.IRegistrationService;
import com.flushbonading.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zlx&zyx
 * @since 2021-04-07
 */
@Service
public class RegistrationServiceImpl extends ServiceImpl<RegistrationMapper, Registration> implements IRegistrationService, RegistrationService {

    @Autowired
    private RegistrationMapper registrationMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private SystemConfigMapper systemConfigMapper;

    /**
     * 登录时进行验证
     */
    @Override
    public Registration Login(final LoginDTO loginDTO) {

        final QueryWrapper<Registration> wrapper = new QueryWrapper<>();
        wrapper.eq("student_number", loginDTO.getStudentNumber());
        //md5加密验证
        wrapper.eq("password", SecureUtil.md5(loginDTO.getPassword()));

        final Registration registration = this.registrationMapper.selectOne(wrapper);
        System.out.println(registration);
        return registration;
    }

    //注册时进行插入
    @Override
    public boolean enroll(final EnrollDTO enrollDTO) {
        //构建对象
        final Registration registration = Registration.builder()
                .class_name(enrollDTO.getClassName())
                .experience(enrollDTO.getExperiences())
                .gender(enrollDTO.getGender())
                .id_card(enrollDTO.getIdCard())
                .name(enrollDTO.getName())
                .password(SecureUtil.md5(enrollDTO.getPassword()))
                .skill(enrollDTO.getSkill())
                .student_number(enrollDTO.getStudentNumber())
                .role_id(4)
                .build();
        System.out.println(registration);
        final int insert = this.registrationMapper.insert(registration);
        if (insert > 0) {
            return true;
        }
        return false;
    }

    /**
     * 通过学号查询姓名
     *
     * @param student_number 学生学号
     * @return 返回学生姓名
     **/
    @Override
    public String getNameByStudentNum(final String student_number) {
        return this.registrationMapper.getNameByStudentNum(student_number);
    }

    @Override
    public List<RegistrationDTO> getRegistrationMessage() {
        return this.registrationMapper.getRegistrationMessage();
    }

    @Override
    public int deleteRegistrationByStudentNum(final String student_number) {
        System.out.println("执行了删除");
        return this.registrationMapper.deleteRegistrationByStudentNum(student_number);
    }

    @Override
    public int updateAdmitByStudentNum(final String student_num, final boolean admit) {
        return this.registrationMapper.updateAdmitByStudentNum(student_num, admit);
    }

    @Override
    public int counts() {
        return this.registrationMapper.counts();
    }

    @Override
    public List<RegistrationDTO> getRegistrationMessageByNum(final String student_number) {
        return this.registrationMapper.getRegistrationMessageByNum(student_number);
    }

    @Transactional
    @Override
    public RespBean insertRegistration(final RegistrationTempDTO registrationTempDTO) {
        final String opened = this.systemConfigMapper.configContent("registration_opened");
        if ("0".equals(opened)) {
            return RespBean.error("报名未开启,请通知管理员！！！");
        }
        System.out.println(this.registrationMapper.registrationCounts(registrationTempDTO.getStudent_number()));
        if (this.registrationMapper.registrationCounts(registrationTempDTO.getStudent_number()) != null) {
            final RespBean error = verifyMsg(registrationTempDTO, 1);
            if (error != null) {
                return error;
            }
            final Integer updateRegistration = this.registrationMapper.updateRegistration(registrationTempDTO);
            System.out.println(updateRegistration);
            if (updateRegistration != null) {
                return RespBean.success("报名信息更新成功");
            }
            return RespBean.error("报名信息更新失败!");
        }
        final RespBean error = verifyMsg(registrationTempDTO, 0);
        if (error != null) {
            return error;
        }
        final Integer i = this.registrationMapper.insertRegistration(registrationTempDTO);
        if (i > 0) {
            return RespBean.success("报名成功");
        }
        return RespBean.error("报名失败!");
    }

    @Override
    public List<RegistrationTempDTO> getRegistrationTempDto() {
        return this.registrationMapper.getRegistrationTempDto();
    }

    @Override
    public List<FileRegistrationDTO> getFileRegistrationDto() {
        return this.registrationMapper.getFileRegistrationDto();
    }

    @Transactional
    @Override
    public RespBean updateRegistrationAdmit(final List<RegistrationTempDTO> registrationTempDTO) {
        final Integer integer = this.registrationMapper.updateRegistrationAdmit(registrationTempDTO);
        System.out.println(integer);
        if (integer >= 0) {
            for (final RegistrationTempDTO tempDTO : registrationTempDTO) {
                if (tempDTO.getAdmit() == 2) {
                    final Integer insertAtStudent = this.registrationMapper.insertAtStudent(tempDTO);
                    if (insertAtStudent > 0) {
                        final int i = this.userMapper.insertAUser(tempDTO.getStudent_number(), "班级内学生");
                        if (i < 0) {
                            return RespBean.error("提交录取信息失败");
                        }
                    } else {
                        return RespBean.error("提交录取信息失败");
                    }
                }
            }
        } else {
            return RespBean.error("提交录取信息失败");
        }
        return RespBean.success("提交录取信息成功");
    }

    @Transactional
    @Override
    public RespBean updateRegistrationData(final RegistrationTempDTO registrationTempDTO) {
        final Integer integer = this.registrationMapper.updateRegistrationData(registrationTempDTO);
        if (integer > 0) {
            return RespBean.success("修改信息成功");
        }
        return RespBean.error("修改信息失败");
    }

    /**
     * 用于检验报名信息的准确性
     */
    private RespBean verifyMsg(final RegistrationTempDTO registrationTempDTO, final int flag) {
        if (registrationTempDTO.getName().length() == 0) {
            return RespBean.error("请填写好姓名！");
        }
        if (registrationTempDTO.getId_card().length() != 18) {
            return RespBean.error("请正确填写身份证号!");
        }
        if (flag == 0 && this.registrationMapper.getCountByIdCard(registrationTempDTO.getId_card()) != null) {
            return RespBean.error("该身份证号已被绑定！！请联系管理员进行修改！！");
        }
        if (registrationTempDTO.getClass_name().length() < 3) {
            return RespBean.error("请填写正确班级!");
        }
        if (registrationTempDTO.getFail_exam().length() == 0) {
            return RespBean.error("请填写挂科情况!");
        }
        if (registrationTempDTO.getGender() != 0 && registrationTempDTO.getGender() != 1) {
            return RespBean.error("请正确填写性别！");
        }
        if (registrationTempDTO.getPolitics_status() != 0 && registrationTempDTO.getPolitics_status() != 1 &&
                registrationTempDTO.getPolitics_status() != 2 && registrationTempDTO.getPolitics_status() != 3) {
            return RespBean.error("请正确填写政治面貌！");
        }
        if (registrationTempDTO.getStudy_direction() != 0 && registrationTempDTO.getStudy_direction() != 3) {
            return RespBean.error("请正确填写学习方向!");
        }
        System.out.println("返回了null");
        return null;
    }

}
