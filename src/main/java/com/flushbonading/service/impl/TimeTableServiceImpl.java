package com.flushbonading.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.TimeTableMapper;
import com.flushbonading.pojo.TimeTable;
import com.flushbonading.service.ITimeTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/4 18:44
 * @since 1.0.0
 */
@Service
public class TimeTableServiceImpl extends ServiceImpl<TimeTableMapper, TimeTable> implements ITimeTableService {
    @Autowired
    private TimeTableMapper timeTableMapper;

    @Override
    public int updatePath(final String path) {
        return this.timeTableMapper.updatePath(path);
    }

    @Override
    public String selectPath() {
        return this.timeTableMapper.selectPath();
    }
}
