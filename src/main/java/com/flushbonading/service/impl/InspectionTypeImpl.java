package com.flushbonading.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.InspectionTypeMapper;
import com.flushbonading.pojo.InspectionType;
import com.flushbonading.service.IInspectionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/10 11:00
 * @since 1.0.0
 */
@Service
public class InspectionTypeImpl extends ServiceImpl<InspectionTypeMapper, InspectionType> implements IInspectionTypeService {
    @Autowired
    private InspectionTypeMapper inspectionTypeMapper;

    @Cacheable(value = "course")
    @Override
    public List<InspectionType> getAll() {
        return this.inspectionTypeMapper.getAll();
    }
}
