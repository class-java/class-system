package com.flushbonading.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.ProjectMapper;
import com.flushbonading.pojo.Project;
import com.flushbonading.pojo.dto.ProjectDTO;
import com.flushbonading.pojo.dto.ProjectListDAO;
import com.flushbonading.pojo.dto.ProjectStudentDTO;
import com.flushbonading.service.IProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
@CacheConfig(cacheNames = "project")
@Service
public class ProjectServiceImpl extends ServiceImpl<ProjectMapper, Project> implements IProjectService {
    @Autowired
    private ProjectMapper projectMapper;

    /**
     * 项目id查询项目名
     *
     * @param project_id
     * @return
     */
    @Cacheable(key = "'selectProjectByProject' + #project_id")
    @Override
    public String selectProjectByProject(final Integer project_id) {
        final QueryWrapper<Project> wrapper = new QueryWrapper<>();
        wrapper.eq("project_id", project_id);
        final Project project = this.projectMapper.selectOne(wrapper);
        return project.getProject_name();
    }


    @Override
    public int getLastProjectID() {
        return this.projectMapper.getLastProjectID();
    }


    @Cacheable(key = "'selectAll'")
    @Override
    public List<ProjectDTO> selectAll() {
        return this.projectMapper.selectAll();
    }

    @CacheEvict
    @Override
    public int insertAProject(final ProjectDTO projectDTO) {
        return this.projectMapper.insertAProject(projectDTO);
    }

    @CacheEvict(allEntries = true)
    @Override
    public int deleteProject(final int project_id) {
        return this.projectMapper.deleteProject(project_id);
    }

    @CacheEvict(allEntries = true)
    @Override
    public int updateGroupName(final int project_id, final String group_name) {
        return this.projectMapper.updateGroupName(project_id, group_name);
    }

    @Cacheable(key = "'selectStudentByID' + #project_id")
    @Override
    public List<ProjectStudentDTO> selectStudentByID(final int project_id) {
        return this.projectMapper.selectStudentByID(project_id);
    }

    @CacheEvict(allEntries = true)
    @Override
    public int deleteAStudent(final String student_number) {
        return this.projectMapper.deleteAStudent(student_number);
    }

    @CacheEvict(allEntries = true)
    @Override
    public int updateProject(final int project_id, final ProjectDTO projectDTO) {
        return this.projectMapper.updateProject(project_id, projectDTO);
    }

    @Override
    public String selectIntroduction(final String student_number) {
        return this.projectMapper.selectIntroduction(student_number);
    }

    @CacheEvict(allEntries = true)
    @Override
    public int updateStudent(final ProjectStudentDTO projectStudentDTO, final int project_id, final int operating_post_id) {
        return this.projectMapper.updateStudent(projectStudentDTO, project_id, operating_post_id);
    }

    @Cacheable(key = "'selectAllProjects'")
    @Override
    public List<ProjectListDAO> selectAllProjects() {
        return this.projectMapper.selectAllProjects();
    }
}
