package com.flushbonading.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.SystemConfigMapper;
import com.flushbonading.pojo.RespBean;
import com.flushbonading.pojo.SystemConfig;
import com.flushbonading.service.SystemConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zk
 * @since 2021-10-11
 */
@Service
public class SystemConfigServiceImpl extends ServiceImpl<SystemConfigMapper, SystemConfig> implements SystemConfigService {

    @Autowired
    private SystemConfigMapper systemConfigMapper;

    @Override
    public RespBean insertConfigInformation(final SystemConfig systemConfig) {
        if (this.systemConfigMapper.configCounts(systemConfig.getType()) == null) {
            final Integer integer = this.systemConfigMapper.insertConfigInformation(systemConfig);
            if (integer > 0) {
                return RespBean.success("配置成功");
            }
        } else {
            final Integer integer = this.systemConfigMapper.updateConfigInformation(systemConfig);
            if (integer >= 0) {
                return RespBean.success("配置成功");
            }
        }
        return RespBean.error("配置失败");
    }
}
