package com.flushbonading.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.GradeMapper;
import com.flushbonading.pojo.Grade;
import com.flushbonading.pojo.dto.GradeDTO;
import com.flushbonading.service.IGradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
@Service
public class GradeServiceImpl extends ServiceImpl<GradeMapper, Grade> implements IGradeService {
    @Autowired
    GradeMapper gradeMapper;

    @Override
    public List<GradeDTO> getList() {
        return this.gradeMapper.getList();
    }
}
