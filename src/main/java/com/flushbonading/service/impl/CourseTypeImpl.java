package com.flushbonading.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.CourseTypeMapper;
import com.flushbonading.pojo.CourseType;
import com.flushbonading.service.ICourseTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/10 10:54
 * @since 1.0.0
 */
@Service
public class CourseTypeImpl extends ServiceImpl<CourseTypeMapper, CourseType> implements ICourseTypeService {
    @Autowired
    private CourseTypeMapper courseTypeMapper;

    @Cacheable(value = "course", key = "'getAll'")
    @Override
    public List<CourseType> getAll() {
        return this.courseTypeMapper.getAll();
    }
}
