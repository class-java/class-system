package com.flushbonading.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flushbonading.mapper.RoleMenuMapper;
import com.flushbonading.pojo.RoleMenu;
import com.flushbonading.service.IRoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements IRoleMenuService {
    @Autowired
    private RoleMenuMapper roleMenuMapper;

    @Override
    public int insertRoleMenu(final int role_id, final List<Integer> menus) {
        int j = -1;
        final int i = this.roleMenuMapper.deleteRoleMenuByID(role_id);
        if (i >= 0)
            j = this.roleMenuMapper.insertRoleMenu(role_id, menus);
        return j;
    }
}
