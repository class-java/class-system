package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.RespBean;
import com.flushbonading.pojo.SystemConfig;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zk
 * @since 2021-10-11
 */
public interface SystemConfigService extends IService<SystemConfig> {
    /**
     * 添加系统的配置信息
     *
     * @param systemConfig 配置信息
     * @return 返回一个判断是否添加成功的值，成功大于0
     */
    RespBean insertConfigInformation(SystemConfig systemConfig);
}
