package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.CourseCharacter;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/10 11:06
 * @since 1.0.0
 */
public interface ICourseCharacterService extends IService<CourseCharacter> {
}
