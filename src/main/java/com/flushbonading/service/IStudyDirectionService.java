package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.StudyDirection;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
public interface IStudyDirectionService extends IService<StudyDirection> {
    /**
     * 学习方向id查询查询学习方向
     *
     * @param study_direction_id
     * @return
     */

    String selectStudyDirection(Integer study_direction_id);
}
