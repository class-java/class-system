package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.Student;
import com.flushbonading.pojo.dto.ProjectStudentDTO;
import com.flushbonading.pojo.dto.StudentDTO;
import com.flushbonading.pojo.dto.StudentMessageDTO;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
public interface IStudentService extends IService<Student> {
    /**
     * 通过学号获得学生信息
     *
     * @param user_id
     * @return
     */
    Student getStudent(String user_id);

    /**
     * 通过学生学号获取学生姓名
     *
     * @param student_number 学生学号
     **/
    String getNameByStudnetNum(String student_number);

    StudentDTO selectStudentMessage(String student_number);

    int updateStudentMessage(StudentDTO studentDTO, String student_number);

    int updateProjectID(int project_id);

    int updateProject(int project_id, ProjectStudentDTO projectStudentDTO);

    int atTheProjectCounts(String student_number);

    Integer getProjectIDByStudentNumber(String student_number);

    String getName(String student_number);

    List<StudentMessageDTO> getAllStudent();

    int deleteAStudent(String student_number);

    int updateStudentMessageByStudentNumber(StudentMessageDTO studentMessageDTO);

    int insertAStudent(StudentMessageDTO studentMessageDTO);

    List<StudentMessageDTO> searchByName(String name);
}
