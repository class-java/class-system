package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.TimeTable;

/**
 * @author zyx 2134208960@qq.com
 * @date 2021/5/4 18:42
 * @since 1.0.0
 */
public interface ITimeTableService extends IService<TimeTable> {
    int updatePath(String path);

    String selectPath();
}
