package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.Registration;
import com.flushbonading.pojo.dto.EnrollDTO;
import com.flushbonading.pojo.dto.LoginDTO;
import com.flushbonading.pojo.dto.RegistrationDTO;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
public interface IRegistrationService extends IService<Registration> {
    //登录接口
    Registration Login(LoginDTO loginDTO);

    //注册接口
    boolean enroll(EnrollDTO enrollDTO);

    /**
     * 通过学号查询姓名
     *
     * @param student_number 学生学号
     * @return 返回学生姓名
     **/
    String getNameByStudentNum(String student_number);

    List<RegistrationDTO> getRegistrationMessage();

    int deleteRegistrationByStudentNum(String student_number);

    int updateAdmitByStudentNum(String student_num, boolean admit);

    int counts();

    List<RegistrationDTO> getRegistrationMessageByNum(String student_number);
}
