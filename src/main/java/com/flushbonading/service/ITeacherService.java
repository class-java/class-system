package com.flushbonading.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.flushbonading.pojo.Teacher;
import com.flushbonading.pojo.dto.TeacherDTO;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zlx
 * @since 2021-04-07
 */
public interface ITeacherService extends IService<Teacher> {

    /**
     * 通过教师号得到教师名
     *
     * @param teacher_id 教师工号
     * @return 返回教师名
     **/
    String getNameByTeacherId(String teacher_id);

    List<TeacherDTO> getAllTeacher();
}
