#java环境
FROM java:8
#指定存放的文件夹
VOLUME /class_system

ADD class_system-0.0.1-SNAPSHOT.jar class_system.jar
#运行jar包
RUN bash -c 'touch /class_system.jar'
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/class_system.jar"]
