# class-system

#### 介绍

此仓库用来存放嵌入式班级管理系统后端代码

#### 软件架构

SpringBoot+MybatisPlus

### 版本说明

JDK:1.8

SpringBoot:2.4.3

druid:1.2.4

mybatis-plus-boot-starter:3.2.0

mybatis-plus-generator:3.3.2

hibernate-validator:6.0.18.Final

### 部署流程

1. 导入doc文件夹里面的class-system.sql到数据库
2. 确认自己的mysql版本 进行修改jar
3. 修改application.yml 里面自己数据库版本对应的jdbc链接
4. 正常启动run ClassSystemApplication.java
5. 跨域信息在 WebConfiguration.java 修改

### 资源下载

- JDK8 http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
- Maven http://maven.apache.org/download.cgi

### 打包发布编译流程

- maven编译安装pom.xml文件即可打包成war

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5. Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
