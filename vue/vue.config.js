let proxyObj = {}
proxyObj['/'] = {
    ws: false,

    target: 'http://localhost:8088',

    changeOrigin: true,

    pathReWrite: {
        '^/': '/'
    }
}

module.exports = {
    assetsDir: 'static',
    parallel: false,
    publicPath: '/',
    devServer: {
        host: 'localhost',
        port: 8080,
        proxy: proxyObj
    }
}
