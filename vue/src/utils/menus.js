import {getRequest} from './api'

export const  initMenu = (router,store)=>{
    if(store.state.routes.length>0){
        return;
    }
    getRequest('/system/config/menu').then(data=>{
        if(data){
            //格式化
            let fmtRoutes=formatRoutes(data);
            //添加router
            router.addRoutes(fmtRoutes);
            //将数据存入vuex
            store.commit('initRoutes',fmtRoutes);
        }
    })
}

export const formatRoutes=(routes)=>{
    let fmtRoutes=[];
    routes.forEach(router=>{
        let{
            path,
            component,
            name,
            iconCls,
            children,
        } =router;
        if(children && children instanceof  Array){
            children = formatRoutes(children);
        }
        let fmRouter = {
            path: path,
            name: name,
            iconCls: iconCls,
            children: children,
            componenta(resolve){
                require(['../views/'+component+'.vue'],resolve);
            }

        }
        fmtRoutes.push(fmRouter)
    });
    return  fmtRoutes;
}
