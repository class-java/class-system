import Vue from 'vue'
import VueRouter from 'vue-router'
import Login3 from "../views/Login-class";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Login3',
        component: Login3
    },
    {
        path: '/main',
        name: 'Main',
        component: () => import(/* webpackChunkName: "about" */ '../views/Main'),
        children: [
            {
                path: '/grade_manegement',
                name: 'Grade_manegement',
                component: () => import(/* webpackChunkName: "about" */ '../views/system/grade_manegement')
            },
            {
                path: '/course_management',
                name: 'Course_manegement',
                component: () => import(/* webpackChunkName: "about" */ '../views/system/course')
            },
            {
                path: '/student',
                name: 'student-system',
                component: () => import(/* webpackChunkName: "about" */ '../views/system/student')
            },
            {
                path: '/teacher',
                name: 'teacher_system',
                component: () => import(/* webpackChunkName: "about" */ '../views/system/teacher')
            },
            {
                path: '/project_management',
                name: 'project_management',
                component: () => import(/* webpackChunkName: "about" */ '../views/system/project')
            }, {
                path: '/role_system',
                name: 'Role_system',
                component: () => import(/* webpackChunkName: "about" */ '../views/system/role')
            },
            {
                path: '/registration_management',
                name: 'registration_management',
                component: () => import(/* webpackChunkName: "about" */ '../views/user/Registration')
            },
            {
                path: '/sign_management',
                name: 'sign_management',
                component: () => import(/* webpackChunkName: "about" */ '../views/system/sign')
            },
            {
                path: '/information_management',
                name: 'information_management',
                component: () => import(/* webpackChunkName: "about" */ '../views/usr/information')
            },
            {
                path: '/score',
                name: 'score',
                component: () => import(/* webpackChunkName: "about" */ '../views/usr/score')
            },
            {
                path: '/schedule',
                name: 'schedule',
                component: () => import(/* webpackChunkName: "about" */ '../views/usr/schedule')
            },
            {
                path: '/introduce',
                name: 'introduce',
                component: () => import(/* webpackChunkName: "about" */ '../views/usr/introduce')
            },
            {
                path: '/project_schedule',
                name: 'project_schedule',
                component: () => import(/* webpackChunkName: "about" */ '../views/usr/project_schedule')
            },
            {
                path: '/idea',
                name: 'idea',
                component: () => import(/* webpackChunkName: "about" */ '../views/usr/idea')
            },
            {
                path: '/help',
                name: 'help',
                component: () => import(/* webpackChunkName: "about" */ '../views/usr/help')
            }


        ]
    },


]

const router = new VueRouter({
    routes
})

export default router
