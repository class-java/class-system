import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'
import router from './router'
import store from './store'


import {deleteRequest, getRequest, postRequest, putRequest} from "./utils/api";

Vue.use(ElementUI);

Vue.config.productionTip = false

Vue.prototype.postRequest = postRequest;

Vue.prototype.getRequest = getRequest
Vue.prototype.putRequest = putRequest
Vue.prototype.deleteRequest = deleteRequest

new Vue({
    router,
    store,
    render: h => h(App),


}).$mount('#app')
