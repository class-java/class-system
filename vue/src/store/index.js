import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    routes: [],
    user_id: "",
    token: ""

  },
  mutations: {
    initRoutes(state,userId){
      state.user_id=userId;
    },
    setToken(state,token){
      state.token=token
    }

  },
  actions: {
  },
  modules: {
  }
})
