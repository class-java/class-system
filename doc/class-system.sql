USE class_system;

DROP TABLE `t_registration`;
CREATE TABLE t_registration(
                               `id` INT(3) NOT NULL AUTO_INCREMENT COMMENT 'id',
                               `student_number` VARCHAR(11) NOT NULL UNIQUE COMMENT '学号',
                               `id_card` VARCHAR(18) NOT NULL UNIQUE COMMENT '身份证号',
                               `name` VARCHAR(10) NOT NULL COMMENT '姓名',
                               `password` VARCHAR(32) NOT NULL COMMENT '密码',
                               `gender` VARCHAR(2) NOT NULL COMMENT '性别',
                               `class_name` VARCHAR(20) NOT NULL COMMENT '班级',
                               `experience` TEXT(256) COMMENT '项目经验',
                               `skill` TEXT(256) COMMENT '技能与获奖',
                               `update_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                               `create_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                               PRIMARY KEY (`id`) USING BTREE
)ENGINE = INNODB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- 增加一个是否被录取字段
ALTER TABLE t_registration ADD `admit` BOOLEAN NOT NULL DEFAULT FALSE COMMENT '是否被录取';

-- 增加字段修改表名
ALTER TABLE t_registration ADD `role_id` INT(2) NOT NULL COMMENT '角色';

-- 学生表
CREATE TABLE t_student(
                          `id` INT(3) NOT NULL AUTO_INCREMENT COMMENT 'id',
                          `grade_id` INT(2) NOT NULL COMMENT '年级id',
                          `student_number` VARCHAR(11) NOT NULL UNIQUE COMMENT '学号',
                          `id_card` VARCHAR(18) NOT NULL UNIQUE COMMENT '身份证号',
                          `name` VARCHAR(10) NOT NULL COMMENT '姓名',
                          `password` VARCHAR(32) NOT NULL COMMENT '密码',
                          `gender` VARCHAR(2) NOT NULL COMMENT '性别',
                          `class_name` VARCHAR(20) NOT NULL COMMENT '班级',
                          `experience` TEXT(256) COMMENT '项目经验',
                          `skill` TEXT(256) COMMENT '技能与获奖',
                          `department` VARCHAR(10) NOT NULL DEFAULT '信息工程系' COMMENT '系别',
                          `study_direction_id` INT(2) COMMENT '学习方向id',
                          `project_id` INT(2) COMMENT '所选项目id',
                          `politics_status` VARCHAR(20) NOT NULL COMMENT '政治面貌',
                          `student_job` VARCHAR(20) NOT NULL COMMENT '学生工作职务',
                          `phone` VARCHAR(11) NOT NULL COMMENT '联系电话',
                          `qq` VARCHAR(20) NOT NULL COMMENT 'qq号',
                          `student_card_number` VARCHAR(10) NOT NULL COMMENT '一卡通号',
                          `bank_card` VARCHAR(20) COMMENT '银行卡号',
                          `address` VARCHAR(100) NOT NULL COMMENT '家庭住址',
                          `postal_code` VARCHAR(10) NOT NULL COMMENT '邮政编码',
                          `parent_name` VARCHAR(10) NOT NULL COMMENT '家长姓名',
                          `parent_phone` VARCHAR(11) NOT NULL COMMENT '家长电话',
                          `update_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                          `create_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                          PRIMARY KEY (`id`) USING BTREE
)ENGINE = INNODB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- 角色表
CREATE TABLE t_role(
                       id INT(2) NOT NULL AUTO_INCREMENT COMMENT 'id',
                       role_id INT(2) UNIQUE NOT NULL COMMENT '角色',
                       role_name VARCHAR(20) NOT NULL COMMENT '角色名',
                       `update_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                       `create_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                       PRIMARY KEY (id) USING BTREE
)ENGINE = INNODB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- 插入数据

INSERT INTO t_role(role_id, role_name)
VALUES (1,'超级用户'),(2,'老师'),(3,'班级内学生');
INSERT INTO t_role(role_id, role_name)
VALUES (4,'刚刚注册的用户');

-- 老师表
DROP TABLE t_teacher;
CREATE TABLE t_teacher(
                          id INT(2) NOT NULL AUTO_INCREMENT COMMENT 'id',
                          teacher_id VARCHAR(20) UNIQUE NOT NULL COMMENT '学工号',
                          teacher_name VARCHAR(20) NOT NULL COMMENT '教师姓名',
                          role_id INT(2) NOT NULL COMMENT '角色',
                          teacger_password VARCHAR(32) NOT NULL COMMENT '教师密码',
                          `update_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                          `create_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                          PRIMARY KEY (id) USING BTREE
)ENGINE = INNODB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- 角色菜单权限表
CREATE TABLE t_role_menu(
                            id INT(2) NOT NULL AUTO_INCREMENT COMMENT 'id',
                            role_id INT(2) NOT NULL COMMENT '角色',
                            menu_id INT(3) NOT NULL COMMENT '菜单id',
                            `update_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                            `create_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                            PRIMARY KEY (id) USING BTREE
)ENGINE = INNODB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- 菜单权限表
CREATE TABLE t_menu(
                       id INT(2) NOT NULL AUTO_INCREMENT COMMENT 'id',
                       menu_id INT(3) UNIQUE NOT NULL COMMENT '菜单id',
                       menu_name VARCHAR(20) NOT NULL COMMENT '菜单名',
                       parent_id INT(2) COMMENT '父菜单id',
                       order_menu INT(2) NOT NULL COMMENT '显示顺序',
                       path VARCHAR(100) NOT NULL COMMENT '跳转路径',
                        `update_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                       `create_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                       PRIMARY KEY (id) USING BTREE
)ENGINE = INNODB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- 学习方向表
    CREATE TABLE t_study_direction(
                                  `id` INT(2) NOT NULL AUTO_INCREMENT COMMENT 'id',
                                  `study_direction_id` INT(2) NOT NULL COMMENT '学习方向id',
                                  `study_direction` VARCHAR(20) NOT NULL COMMENT '学习方向',
                                  `update_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                  `create_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                  PRIMARY KEY (`id`) USING BTREE
)ENGINE = INNODB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- 插入学习方向数据
INSERT INTO t_study_direction(study_direction_id, study_direction)
VALUES (1,'嵌入式系统软件开发组'),(2,'嵌入式系统硬件开发组'),(3,'应用端软件开发组');

-- 项目表
CREATE TABLE t_project(
                          `id` INT(2) NOT NULL AUTO_INCREMENT COMMENT 'id',
                          `project_id` INT(2) UNIQUE NOT NULL COMMENT 'project_id',
                          `project_name` VARCHAR(50) NOT NULL COMMENT 'project_name',
                          `project_brief` TEXT(500) NOT NULL COMMENT '项目简介',
                          `project_progress` VARCHAR(50) NOT NULL COMMENT '项目进度',
                          `project_schedule` TEXT(250) NOT NULL COMMENT '项目计划',
                          `project_manager_id` INT(2) COMMENT '项目经理id',
                          `project_secretary_id` INT(2) COMMENT '项目秘书id',
                          `update_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                          `create_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                          PRIMARY KEY (`id`) USING BTREE
)ENGINE = INNODB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- 课程表
CREATE TABLE t_course(
                         `id` INT(2) NOT NULL AUTO_INCREMENT COMMENT 'id',
                         `course_id` INT(2) NOT NULL UNIQUE COMMENT '课程id',
                         `study_direction_id` INT(2) COMMENT '学习方向id',
                         `course_name` INT(2) NOT NULL COMMENT '课程名',
                         `update_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                         `create_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                         PRIMARY KEY (`id`) USING BTREE
)ENGINE = INNODB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- 成绩表
CREATE TABLE t_score(
                        `id` INT(2) NOT NULL AUTO_INCREMENT COMMENT 'id',
                        `course_id` INT(2) NOT NULL COMMENT '课程id',
                        `student_number` VARCHAR(11) NOT NULL COMMENT '学号',
                        `grade` FLOAT(4,1) UNSIGNED NOT NULL COMMENT '成绩',
                        `update_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                        `create_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                        PRIMARY KEY (`id`) USING BTREE
)ENGINE = INNODB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci;

ALTER TABLE t_score CHANGE grade score FLOAT(4,1) UNSIGNED NOT NULL COMMENT '成绩';
DESC t_score;
-- 年级表
CREATE TABLE t_grade(
                        `id` INT(2) NOT NULL AUTO_INCREMENT COMMENT 'id',
                        `grade_id` INT(2) NOT NULL UNIQUE COMMENT '年级id',
                        `grade_name` VARCHAR(10) NOT NULL COMMENT '年级名',
                        `update_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                        `create_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                        PRIMARY KEY (`id`) USING BTREE
)ENGINE = INNODB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- 用户表
CREATE TABLE t_user(
                       id INT(2) NOT NULL AUTO_INCREMENT COMMENT 'id',
                       user_id VARCHAR(11) UNIQUE NOT NULL COMMENT '用户id',
                       PASSWORD VARCHAR(40)  NOT NULL COMMENT '用户密码',
                       role_id INT(2)  NOT NULL COMMENT '用户权限',
                       nature VARCHAR(0) NOT NULL COMMENT '属性',
                       `update_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                       `create_time` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                       PRIMARY KEY (id) USING BTREE
)ENGINE = INNODB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci;
